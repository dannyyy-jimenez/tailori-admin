// Technical imports

import 'react-native-gesture-handler';
import React from 'react';
import * as Font from 'expo-font';
import { StatusBar, Vibration, View, Text } from 'react-native';
import { NavigationContainer, DefaultTheme, CommonActions } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { MaterialIcons, Ionicons } from '@expo/vector-icons';
import * as SplashScreen from 'expo-splash-screen';
import * as SecureStore from 'expo-secure-store';
import * as Permissions from 'expo-permissions';
import Constants from 'expo-constants';
import * as ExpoNotifications from 'expo-notifications';
import Localization from './localization/config';

import API from './Api';

const styles = require('./Styles');

const AppTheme = {
  ...DefaultTheme,
  colors: {
    ...DefaultTheme.colors,
    background: styles.Secondary,
    primary: styles.Primary
  },
};


// Components
import Login from './components/Login';
import Orders from './components/Orders';
import Order from './components/Order';
import Payout from './components/Payout';

import Validate from './components/Validate';
import Update from './components/Update';

import Releases from './components/Releases';
import Release from './components/Release';
import Designs from './components/Designs';
import Design from './components/Design';

import Brands from './components/Brands';
import Brand from './components/Brand';
import Users from './components/Users';
import User from './components/User';

import Dashboard from './components/Dashboard';

const Tab = createBottomTabNavigator();
const Stack = createStackNavigator();
const OrdersStack = createStackNavigator();
const BrandsStack = createStackNavigator();
const ReleasesStack = createStackNavigator();
const DashboardStack = createStackNavigator();

// Routes
function OrdersHolder({route}) {
  return (
    <OrdersStack.Navigator initialRouteName="Orders">
      <OrdersStack.Screen name="Orders" initialParams={{session: route.params.session}} component={Orders} options={{headerShown: false}} />
      <OrdersStack.Screen name="Design" initialParams={{session: route.params.session}} component={Design} options={{headerShown: false}} />
      <OrdersStack.Screen name="User" initialParams={{session: route.params.session}} component={User} options={{headerShown: false}} />
      <OrdersStack.Screen name="Brand" initialParams={{session: route.params.session}} component={Brand} options={{headerShown: false}} />
      <OrdersStack.Screen name="Order" initialParams={{session: route.params.session}} component={Order} options={{headerShown: false}} />
      <OrdersStack.Screen name="Payout" initialParams={{session: route.params.session}} component={Payout} options={{headerShown: false}} />
      <OrdersStack.Screen name="Update" initialParams={{session: route.params.session}} component={Update} options={{headerShown: false}} />
    </OrdersStack.Navigator>
  )
}

function DashboardHolder({route}) {
  return (
    <DashboardStack.Navigator initialRouteName="Dashboard">
      <DashboardStack.Screen name="Dashboard" initialParams={{session: route.params.session}} component={Dashboard} options={{headerShown: false}} />
      <DashboardStack.Screen name="Users" initialParams={{session: route.params.session}} component={Users} options={{headerShown: false}} />
      <DashboardStack.Screen name="User" initialParams={{session: route.params.session}} component={User} options={{headerShown: false}} />
    </DashboardStack.Navigator>
  )
}

function BrandsHolder({route}) {
  return (
    <BrandsStack.Navigator initialRouteName="Brands">
      <BrandsStack.Screen name="Brands" initialParams={{session: route.params.session}} component={Brands} options={{headerShown: false}} />
      <BrandsStack.Screen name="Users" initialParams={{session: route.params.session}} component={Users} options={{headerShown: false}} />
      <BrandsStack.Screen name="User" initialParams={{session: route.params.session}} component={User} options={{headerShown: false}} />
      <BrandsStack.Screen name="Brand" initialParams={{session: route.params.session}} component={Brand} options={{headerShown: false}} />
    </BrandsStack.Navigator>
  )
}

function ReleasesHolder({route}) {
  return (
    <ReleasesStack.Navigator initialRouteName="Releases">
        <ReleasesStack.Screen name="Releases" initialParams={{session: route.params.session}} component={Releases} options={{headerShown: false}} />
        <ReleasesStack.Screen name="Designs" initialParams={{session: route.params.session}} component={Designs} options={{headerShown: false}} />
        <ReleasesStack.Screen name="Design" initialParams={{session: route.params.session}} component={Design} options={{headerShown: false}} />
        <ReleasesStack.Screen name="User" initialParams={{session: route.params.session}} component={User} options={{headerShown: false}} />
        <ReleasesStack.Screen name="Brand" initialParams={{session: route.params.session}} component={Brand} options={{headerShown: false}} />
        <ReleasesStack.Screen name="Release" initialParams={{session: route.params.session}} component={Release} options={{headerShown: false}} />
        <ReleasesStack.Screen name="Validate" initialParams={{session: route.params.session}} component={Validate} options={{headerShown: false}} />
    </ReleasesStack.Navigator>
  )
}

function Home({navigation, route}) {
  const { logout } = React.useContext(AuthContext);

  return (
    <Tab.Navigator initialRouteName="Dashboard" tabBarOptions={TabBarOptions} screenOptions={TabBarScreenOptions}>
      <Tab.Screen name='Dashboard' initialParams={{session: route.params.session}} component={DashboardHolder} />
      <Tab.Screen name='Orders' initialParams={{session: route.params.session}} component={OrdersHolder} />
      <Tab.Screen name='Releases' initialParams={{session: route.params.session}} component={ReleasesHolder} />
      <Tab.Screen name='Brands' initialParams={{session: route.params.session}} component={BrandsHolder} />
    </Tab.Navigator>
  );
}

ExpoNotifications.setNotificationHandler({
  handleNotification: async () => ({
    shouldShowAlert: true,
    shouldPlaySound: true,
    shouldSetBadge: true
  }),
});

const AuthContext = React.createContext();

function App() {

  const [notification, setNotification] = React.useState(false);
  const notificationListener = React.useRef();
  const [state, dispatch] = React.useReducer(
    (prevState, action) => {
      switch (action.type) {
        case 'RESTORE_TOKEN':
          return {
            ...prevState,
            authenticated: action.authenticated,
            session: action.session,
            appIsReady: true
          };
        case 'LOG_IN':
          return {
            ...prevState,
            authenticated: true,
            session: {
              ...prevState.session,
              crypto: action.crypto,
              name: action.name
            }
          };
        case 'LOG_OUT':
          return {
            ...prevState,
            authenticated: false,
            session: {
              crypto: '',
              name: '',
              socket: ''
            },
          };
        case 'NOTIFICATION_TOKEN':
          return {
            ...prevState,
            session: {
              ...prevState.session,
              socket: action.token
            }
          }
      }
    },
    {
      appIsReady: false,
      authenticated : false,
      session: {
        crypto: '',
        name: '',
        socket: ''
      },
      notification: {}
    }
  );

  const authContext = React.useMemo(() => ({
    login: async ({name, crypto, pfp}) => {
      await SecureStore.setItemAsync('_DXAC', crypto);
      await SecureStore.setItemAsync('_DXAN', name);
      dispatch({type: "LOG_IN", name, crypto, pfp})
      if (state.session.socket) {
        const body = {auth: state.session.crypto, token: state.session.socket};
        await API.post('/settings/notifications', body);
      }
    },
    logout: async (data) => {
      if (state.session.socket) {
        const body = {auth: state.session.crypto, token: state.session.socket};
        const notifRemoveReq = await API.post('/settings/notifications/remove', body);
      }
      dispatch({type: "LOG_OUT"})
      await SecureStore.deleteItemAsync('_DXAC');
      await SecureStore.deleteItemAsync('_DXAN');
    }
  }), []);

  React.useEffect(() => {
    ExpoNotifications.setBadgeCountAsync(0);
    const load = async () => {
      try {
        await SplashScreen.preventAutoHideAsync();
      } catch (e) {
        //console.log(e);
      }

      await prepareResources();
      notificationListener.current = ExpoNotifications.addNotificationReceivedListener(notification => {
        Vibration.vibrate();
        ExpoNotifications.setBadgeCountAsync(0);
      });

    }
    load();

    return () => {
      ExpoNotifications.removeNotificationSubscription(notificationListener);
    };
  }, []);

  React.useEffect(() => {
    SplashScreen.hideAsync();
  }, [state.appIsReady])

   const prepareResources = async () => {
     await Font.loadAsync({
       'Manrope': require('./assets/fonts/ManropeRegular.otf'),
    });
    let session = await loadSession();
    dispatch({type: 'RESTORE_TOKEN', ...session});
    try {
      const token = await registerForPushNotificationsAsync();
      if (token !== "") {
        if (session.authenticated) {
          const body = {auth: session.session.crypto, token};
          const res = await API.post('settings/notifications', body);
        }
        dispatch({type: 'NOTIFICATION_TOKEN', token})
      }
    } catch (e) {
      console.log(e);
    }
  };

  const loadSession = async () => {
    let crypto = await SecureStore.getItemAsync('_DXAC');
    let name = await SecureStore.getItemAsync('_DXAN');
    let authenticated = (crypto && name) !== null;

    return {
      authenticated: authenticated,
      session: {
        crypto: authenticated ? crypto : '',
        name: authenticated ? name : '',
        socket: state.session.socket
      }
    }
  }

  return (

    <AuthContext.Provider value={authContext}>
       <StatusBar barStyle="dark-content" />
       <NavigationContainer theme={AppTheme}>
         <Stack.Navigator screenOptions={{gestureEnabled: false}}>
           {
             state.authenticated ? (
               <>
                 <Stack.Screen name="Home" component={Home} initialParams={{session: state.session}} options={{headerShown: false}}/>
                 <Stack.Screen name="Logout" options={{headerShown: false}}>
                   {props => {
                     React.useEffect(() => {
                       authContext.logout(props.route.params);
                     }, [])
                     return <View style={{background: styles.Secondary, height: '100%', width: '100%'}}></View>
                   }}
                 </Stack.Screen>
               </>
             ) :
             (
               <>
                 <Stack.Screen name="Login" component={Login} options={{headerShown: false}}/>
                 <Stack.Screen name="Auth" options={{headerShown: false}}>
                   {props => {
                     React.useEffect(() => {
                       authContext.login(props.route.params);
                     }, [])
                     return <View style={{background: styles.Secondary, height: '100%', width: '100%'}}></View>
                   }}
                 </Stack.Screen>
               </>
             )
           }
         </Stack.Navigator>
       </NavigationContainer>
     </AuthContext.Provider>
  )
}

async function registerForPushNotificationsAsync() {
  let token;
  if (Constants.isDevice) {
    const { status: existingStatus } = await Permissions.getAsync(Permissions.NOTIFICATIONS);
    let finalStatus = existingStatus;
    if (existingStatus !== 'granted') {
      const { status } = await Permissions.askAsync(Permissions.NOTIFICATIONS);
      finalStatus = status;
    }
    if (finalStatus !== 'granted') {
      alert('Failed to get push token for push notification!');
      return;
    }
    token = (await ExpoNotifications.getExpoPushTokenAsync({experienceId: '@dannyyy.jimenez/tailorideveloper'})).data;
  } else {
    alert('Must use physical device for Push Notifications');
  }

  if (Platform.OS === 'android') {
    ExpoNotifications.setNotificationChannelAsync('default', {
      name: 'default',
      importance: ExpoNotifications.AndroidImportance.MAX,
      vibrationPattern: [0, 250, 250, 250],
      lightColor: '#FFDB113B',
    });
  }

  return token;
};

const TabBarOptions = {
  activeTintColor: styles.Primary,
  inactiveTintColor: '#CCC',
  activeBackgroundColor: styles.Secondary,
  inactiveBackgroundColor: styles.Secondary,
  showLabel: true,
  showIcon: true,
  style: {
    backgroundColor: styles.Secondary,
    borderTopWidth: 0
  }
}

const TabBarScreenOptions = ({route}) => ({
  tabBarLabel: Localization.t(route.name.toLowerCase()),
  tabBarIcon: ({focused, color, size}) => {
    let iconName;

    if (route.name === 'Orders') {
      iconName = "md-layers";
    } else if (route.name === 'Brands') {
      iconName = "md-pricetags";
    } else if (route.name === 'Releases') {
      iconName = "ios-shirt";
    } else if (route.name === 'Dashboard') {
      iconName = "md-pulse";
    }

    return <Ionicons name={iconName} size={28} color={color} />;
  }
})

export default App;
