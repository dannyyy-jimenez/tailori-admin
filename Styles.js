import { StyleSheet, Platform } from 'react-native';

let Primary = '#DB113B'; // 4ECDC4 0B3954 FF6F59 43AA8B 5CC8FF
let PrimaryOpaque = "rgba(219, 17, 59, 0.7)";
let Secondary = '#FFFFFF'; // FFF -> DDD
let Tertiary =  '#111111'; //111111 -> 222222
let SecondaryTint = '#F9F9F9'; //
let StatusBar = 'dark-content';

const Styles = StyleSheet.create({
  miniText: {
    fontSize: 10
  },
  tinyText: {
    fontSize: 12
  },
  paddedSides: {
    paddingLeft: 10,
    paddingRight: 10
  },
  baseText: {
    fontSize: 15,
    letterSpacing: 0.6
  },
  subHeaderText: {
    fontSize: 20
  },
  headerText: {
    fontSize: 28
  },
  megaText: {
    fontSize: 36
  },
  manropeText: {
    fontFamily: 'Manrope'
  },
  primary: {
    color: Primary
  },
  secondary: {
    color: Secondary
  },
  tertiary: {
    color: Tertiary
  },
  bold: {
    fontWeight: '700'
  },
  spacer: {
    flex: 1,
    flexGrow: 1
  },
  baseInput: {
    ...Platform.select({
      web: {
        caretColor: Primary,
        outlineColor: Secondary,
      }
    }),
    paddingLeft: 4,
    paddingRight: 4,
    fontSize: 16,
    letterSpacing: 1.3,
    textAlign: 'center'
  },
  inputMultiline: {
    lineHeight: 26,
    height: 96
  },
  baseInputHeader: {
    paddingLeft: 4,
    paddingRight: 4,
    width: '85%',
  },
  baseInputContainer: {
    display: 'flex',
    flexDirection: 'row',
    width: '85%'
  },
  inputMulti: {
    width: 'auto',
    flexGrow: 1
  },
  alignLeft: {
    textAlign: 'left',
  },
  inputHasHeader: {
    width: '85%',
    height: 46
  },
  filledInput: {
    backgroundColor: SecondaryTint,
    width: '85%',
    height: 48,
    borderRadius: 5
  },
  disabled: {
    opacity: 0.3
  },
  roundedButton: {
    width: '85%',
    height: 48,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 25,
    marginTop: 4,
    marginBottom: 4
  },
  filled: {
    backgroundColor: Primary
  },
  clear: {
    backgroundColor: Secondary
  },
  centerText: {
    textAlign: 'center'
  },
  opaque: {
    opacity: 0.8
  },
  defaultTabContainer: {
    height: '100%',
    width: '100%',
    flex: 1,
    display: 'flex',
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    flexDirection: 'column'
  },
  defaultTabHeader: {
    height: 64,
    width: '100%',
    display: 'flex',
    paddingLeft: 15,
    paddingRight: 15,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row'
  },
  defaultTabContent: {
    flexGrow: 1,
    flex: 1,
    width: '98%',
    marginLeft: '1%',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center'
  },
  rowTabContent: {
    flexGrow: 1,
    flex: 1,
    width: '98%',
    marginLeft: '1%',
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'space-around',
    alignItems: 'center'
  },
  defaultTabScrollContent: {
    flexGrow: 1,
    flex: 1,
    width: '100%'
  },
  paddedWidth: {
    width: '80%',
    marginLeft: '10%'
  },
  line: {
    height: 1,
    width: '100%',
    marginTop: 20,
    marginBottom: 20,
    backgroundColor: Tertiary,
    opacity: 0.2,
    borderRadius: 4
  },
  actionListItem: {
    height: 45,
    marginTop: 2.5,
    marginBottom: 2.5,
    paddingTop: 5,
    paddingBottom: 5
  },
  baseSwitchContainer: {
    display: 'flex',
    flexDirection: 'row',
    width: '85%',
    paddingLeft: 4,
    paddingRight: 4,
    height: 40,
    alignItems: 'center'
  },
  baseSwitchHeader: {
    flexGrow: 1
  },
  caps: {
    textTransform: 'uppercase'
  },
  cardNumber: {
    letterSpacing: 1.3
  },
  cardContainer: {
    width: 350,
    height: 220,
    backgroundColor: Primary,
    borderRadius: 15,
    display: 'flex',
    flexDirection: 'column',
    paddingLeft: 20,
    paddingRight: 20,
    paddingTop: 18,
    paddingBottom:18
  },
  defaultRowContainer: {
    flexDirection: 'row'
  },
  fullWidth: {
    width: '100%'
  },
  marginWidth: {
    width: '90%',
    marginLeft: '5%',
    marginRight: '5%'
  },
  defaultBrandLogo: {
    width: '20%',
    height: 80,
    resizeMode: 'contain'
  },
  defaultColumnContainer: {
    flexDirection: 'column'
  },
  fullHeight: {
    height: '100%'
  },
  center: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center'
  },
  defaultZoomImage: {
    height: '55%',
    width: '100%',
    resizeMode: 'contain'
  },
  defaultLandingImage: {
    height: 150,
    width: 150,
    resizeMode: 'contain'
  },
  defaultLandingImageSmall: {
    height: 80,
    width: 80,
    resizeMode: 'contain'
  },
  zoomColor: {
    height: 24,
    width: 24,
    borderRadius: 12,
    backgroundColor: SecondaryTint,
    borderColor: Tertiary,
    borderWidth: 2,
    marginRight: 4
  },
  defaultStoreCard: {
    minHeight: 280,
    width: 350,
    margin: 10,
  },
  defaultSCImage: {
    height: 200,
    width: '100%',
    resizeMode: 'contain'
  },
  defaultSCContent: {
    minHeight: 80,
    width: '100%',
    backgroundColor: SecondaryTint,
    borderRadius: 10,
    padding: 5,
    paddingLeft: 10,
    paddingRight: 10
  },
  backArrow: {
    padding: 8
  },
  defaultAbsolute: {
    position: 'absolute',
    zIndex: 1000,
    top: 0,
    left: 0,
    backgroundColor: Secondary
  },
  statCard: {
    width: 150,
    margin: 5,
    marginTop: 20,
    height: 120,
    borderRadius: 5,
    padding: 20,
    backgroundColor: SecondaryTint
  },
  defaultZoomImageContainer: {
    height: 380,
    width: '100%',
  },
  defaultZoomImage: {
    height: 380,
    width: '100%',
    resizeMode: 'contain'
  },
  defaultZoomMultiImage: {
    height: '100%',
    width: '100%',
    resizeMode: 'contain'
  },
  wrap: {
    flexWrap: 'wrap'
  }
});

export {
  Styles,
  Primary,
  PrimaryOpaque,
  Secondary,
  SecondaryTint,
  Tertiary
}
