const formats = require('../components/Formats').Formats;

export default class Design {

  constructor(identifier, creator, name, cover, type, date, deleted) {
    this.identifier = identifier;
    this.creator = creator;
    this.name = name;
    this.cover = cover;
    this.type = type;
    this.date = date;
    this.deleted = deleted;
  }

  getIdentifier() {
    return this.identifier;
  }

  getCreator() {
    return this.creator;
  }

  getName() {
    return this.name;
  }

  getCover() {
    return this.cover;
  }

  getType() {
    return formats.getType(this.type);
  }

  getDate() {
    return this.date;
  }

  wasDeleted() {
    return this.deleted;
  }
}
