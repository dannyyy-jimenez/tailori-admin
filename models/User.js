export default class User {

  constructor(id, name, brand, since, designs, deleted) {
    this.id = id;
    this.name = name;
    this.brand = brand;
    this.since = since;
    this.designs = designs;
    this.deleted = deleted;
  }

  getID() {
    return this.id;
  }

  getName() {
    return this.name;
  }

  getBrand() {
    return this.brand;
  }

  getSince() {
    return this.since;
  }

  getDesigns() {
    return this.designs;
  }

  wasDeleted() {
    return this.deleted;
  }
}
