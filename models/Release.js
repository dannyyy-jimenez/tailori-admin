export default class Release {

  constructor(identifier, cover, name, price, date, approved) {
    this.identifier = identifier;
    this.name = name;
    this.cover = cover;
    this.price = price;
    this.date = date;
    this.approved = approved;
  }

  getIdentifier() {
    return this.identifier;
  }

  getCover() {
    return this.cover;
  }

  getName() {
    return this.name;
  }

  getPrice() {
    return this.price;
  }

  getDate() {
    return this.date;
  }

  wasApproved() {
    return this.approved;
  }
}
