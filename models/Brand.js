export default class Brand {

  constructor(id, identifier, name, logo, payouts, distribution, hasReleases) {
    this.id = id;
    this.identifier = identifier;
    this.name = name;
    this.logo = logo;
    this.payouts = payouts;
    this.distribution = distribution;
    this.hasReleases = hasReleases;
  }

  getID() {
    return this.id;
  }

  getIdentifier() {
    return this.identifier;
  }

  getName() {
    return this.name;
  }

  getLogo() {
    return this.logo;
  }

  getPayouts() {
    return this.payouts;
  }

  getDistribution() {
    return this.distribution;
  }
}
