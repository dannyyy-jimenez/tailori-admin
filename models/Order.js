import Localization from '../localization/config';

export default class Order {

  constructor(identifier, buyer, items, total, date, daysLeft, fulfilled) {
    this.identifier = identifier;
    this.buyer = buyer;
    this.items = items;
    this.total = total;
    this.date = date;
    this.fulfilled = fulfilled;
    this.daysLeft = daysLeft;
  }

  getIdentifier() {
    return this.identifier;
  }

  getDaysLeft() {
    return this.daysLeft;
  }

  getDaysLeftFormatted() {
    if (this.daysLeft === 1) return Localization.t('dayLeft');
    if (this.daysLeft > 1) return Localization.t('daysLeft', {days: this.daysLeft});

    if (this.daysLeft === -1) return Localization.t('dayLate');

    return Localization.t('daysLate', {days: (this.daysLeft * -1)});
  }

  getBuyer() {
    return this.buyer;
  }

  getItems() {
    return this.items;
  }

  getItemsFormat() {
    return this.items === 1 ? `1 ${Localization.t('item')}` : `${this.items} ${Localization.t('items')}`;
  }

  getTotal() {
    return this.total;
  }

  getDate() {
    return this.date;
  }

  wasFulfilled() {
    return this.fulfilled;
  }
}
