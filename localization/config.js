import * as Localization from 'expo-localization';
import i18n from 'i18n-js';
import EN from './en';
import ES from './es';

i18n.translations = {
  en: EN,
  es: ES
};

i18n.locale = Localization.locale;
i18n.defaultLocale = "en";
i18n.fallbacks = true;

export default i18n;
