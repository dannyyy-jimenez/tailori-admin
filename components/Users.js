import React from 'react';
import { View, SafeAreaView, Text, ScrollView, RefreshControl, ActivityIndicator, StyleSheet, TextInput, TouchableOpacity } from 'react-native';
import { Ionicons, MaterialIcons } from '@expo/vector-icons';
import ActionSheet from "react-native-actions-sheet";
import Localization from '../localization/config';

import API from '../Api';
import User from '../models/User';

const stylesheet = require('../Styles');
const styles = stylesheet.Styles;
const formats = require('./Formats').Formats;

const actionSheetRef = React.createRef();

export default function Users({navigation, route}) {
  const SESSION = route.params.session;

  const [searchQuery, setSearchQuery] = React.useState('');
  const [refreshing, setRefreshing] = React.useState(false);
  const [isLoading, setIsLoading] = React.useState(true);
  const [users, setUsers] = React.useState([]);
  const [baseUsers, setBaseUsers] = React.useState([]);
  const [hasPermission, setHasPermission] = React.useState(false);

  const [sortBy, setSortBy] = React.useState('newest');
  const [displayDeleted, setDisplayDeleted] = React.useState(true);

  const load = async () => {
    setIsLoading(true);

    try {
      const res =  await API.get('users', {auth: SESSION.crypto, sortBy, displayDeleted});
      if (res.isError && res.response === 'NO_PERMISSION') throw 'permission';

      if (res.isError) throw 'error';

      setHasPermission(true);
      setBaseUsers(res.data._u.map(user => new User(user.id, user.name, user.brand, user.since, user.designs, user.deleted)));
      setUsers(res.data._u.map(user => new User(user.id, user.name, user.brand, user.since, user.designs, user.deleted)));
      setIsLoading(false);
    } catch (e) {
      if (e === 'permission') {
        setHasPermission(false);
      }
      setIsLoading(false);
    };
  };

  const refresh = async() => {
    setRefreshing(true);
    await load();
    setRefreshing(false);
  }

  React.useEffect(() => {
    load();
  }, [sortBy, displayDeleted]);

  React.useEffect(() => {
    if (searchQuery.replace(/ /g, '') === '') {
      return;
      setUsers(baseUsers.slice());
    }
    setUsers(baseUsers.filter((user) => {
      let likeRegex = new RegExp(searchQuery, 'gi');
      return likeRegex.test(user.getID()) || likeRegex.test(user.getName()) || likeRegex.test(user.getBrand());
    }));
  }, [searchQuery]);

  return (
    <SafeAreaView style={styles.defaultTabContainer}>
      <View style={styles.defaultTabHeader}>
        <TouchableOpacity
          style={styles.backArrow}
          onPress={() => navigation.goBack()}
          underlayColor='#fff'>
          <Ionicons name="ios-arrow-back" size={24} color={stylesheet.Tertiary} />
        </TouchableOpacity>
        <View style={styles.spacer}></View>
        <TextInput style={[styles.baseInput, styles.filledInput, styles.tertiary]} value={searchQuery} onChangeText={value => setSearchQuery(value)} keyboardType="default" placeholder={Localization.t('searchUsers')} placeholderTextColor="#555555" returnKeyType="search" selectionColor="#DB113B" textContentType="none" />
        <View style={styles.spacer}></View>
        <TouchableOpacity
          onPress={() => actionSheetRef.current?.setModalVisible(true)}
          underlayColor='#fff'>
          <MaterialIcons name="sort" size={24} color={stylesheet.Primary} />
        </TouchableOpacity>
      </View>
      {
        isLoading &&
        <View style={[styles.defaultLoader, styles.fullWidth, styles.fullHeight, styles.defaultColumnContainer]}>
          <View style={{flex: 0.1}}></View>
          <ActivityIndicator size="small" color={stylesheet.Primary} />
        </View>
      }
      <ScrollView style={styles.defaultTabScrollContent} contentContainerStyle={{alignItems: 'center', justifyContent: 'flex-start', width: '90%', marginLeft: '5%'}} refreshControl={<RefreshControl refreshing={refreshing} tintColor={stylesheet.Primary} colors={[stylesheet.Primary]} onRefresh={load} />}>
        <View style={{flex: 0.05}}></View>
        {
          !hasPermission &&
          <>
            <View style={styles.spacer}></View>
            <Ionicons name="md-lock" size={48} color={stylesheet.Primary} />
            <View style={{flex: 0.05}}></View>
            <Text style={[styles.centerText, styles.primary, styles.bold, styles.baseText]}>{Localization.t('NO_PERMISSION')}</Text>
            <View style={styles.spacer}></View>
          </>
        }
        {
          users.map((user) => {
            return (
              <TouchableOpacity key={user.getID()} onPress={() => navigation.navigate('User', {session: SESSION, identifier: user.getID(), name: user.getName()})} style={[Styles.itemContainer, !user.wasDeleted() ? {opacity: 1} : styles.opaque]}>
                <View style={styles.defaultRowContainer}>
                  <Text style={[styles.baseText, styles.bold, styles.tertiary]}>{user.getName()}</Text>
                  <View style={styles.spacer}></View>
                  <Text style={[styles.tinyText, styles.opaque, styles.tertiary]}>{user.getSince()}</Text>
                </View>
                <View style={styles.spacer}></View>
                <View style={styles.defaultRowContainer}>
                  {
                    user.getBrand() !== "" &&
                    <Text style={[styles.tinyText, styles.bold, styles.opaque, styles.tertiary]}>{user.getBrand()}</Text>
                  }
                  {
                    user.getBrand() === "" &&
                    <Text style={[styles.tinyText, styles.bold, styles.opaque, styles.tertiary]}>{user.getID()}</Text>
                  }
                  <View style={styles.spacer}></View>
                  <Text style={[styles.tinyText, styles.opaque, styles.tertiary]}>{user.getDesigns()}</Text>
                </View>
              </TouchableOpacity>
            )
          })
        }
      </ScrollView>
      <ActionSheet containerStyle={{paddingBottom: 20, backgroundColor: stylesheet.Secondary}} indicatorColor={stylesheet.Primary} gestureEnabled={true} ref={actionSheetRef}>
        <View>
          <Text style={[styles.baseText, styles.fullWidth, styles.bold, styles.centerText, styles.tertiary, {marginTop: 10}]}>Filters</Text>
          <View style={styles.line}></View>
          <Text style={[styles.baseText, styles.marginWidth, styles.bold, styles.tertiary, {marginTop: 10, marginBottom: 10}]}>Sort By</Text>
          <View style={[styles.paddedWidth, styles.defaultColumnContainer]}>
            <TouchableOpacity onPress={() => setSortBy('newest')} disabled={sortBy === 'newest'} style={[styles.defaultRowContainer, styles.actionListItem, sortBy === 'newest' ? styles.disabled : {}]}>
              <Text style={[styles.spacer, styles.baseText, styles.tertiary]}>{Localization.t('newest')}</Text>
              {
                sortBy === 'newest' &&
                <Ionicons name="md-checkmark" size={18} color={stylesheet.Secondary} />
              }
            </TouchableOpacity>
          </View>
          <View style={[styles.paddedWidth, styles.defaultColumnContainer]}>
            <TouchableOpacity onPress={() => setSortBy('oldest')} disabled={sortBy === 'oldest'} style={[styles.defaultRowContainer, styles.actionListItem, sortBy === 'oldest' ? styles.disabled : {}]}>
              <Text style={[styles.spacer, styles.baseText, styles.tertiary]}>{Localization.t('oldest')}</Text>
              {
                sortBy === 'oldest' &&
                <Ionicons name="md-checkmark" size={18} color={stylesheet.Secondary} />
              }
            </TouchableOpacity>
          </View>

          <Text style={[styles.baseText, styles.marginWidth, styles.bold, styles.tertiary, {marginTop: 20, marginBottom: 10}]}>Display Deleted</Text>
          <View style={[styles.paddedWidth, styles.defaultColumnContainer]}>
            <TouchableOpacity onPress={() => setDisplayDeleted(true)} disabled={displayDeleted} style={[styles.defaultRowContainer, styles.actionListItem, displayDeleted ? styles.disabled : {}]}>
              <Text style={[styles.spacer, styles.baseText, styles.tertiary]}>{Localization.t('true')}</Text>
              {
                displayDeleted &&
                <Ionicons name="md-checkmark" size={18} color={stylesheet.Secondary} />
              }
            </TouchableOpacity>
          </View>
          <View style={[styles.paddedWidth, styles.defaultColumnContainer]}>
            <TouchableOpacity onPress={() => setDisplayDeleted(false)} disabled={!displayDeleted} style={[styles.defaultRowContainer, styles.actionListItem, !displayDeleted ? styles.disabled : {}]}>
              <Text style={[styles.spacer, styles.baseText, styles.tertiary]}>{Localization.t('false')}</Text>
              {
                !displayDeleted &&
                <Ionicons name="md-checkmark" size={18} color={stylesheet.Secondary} />
              }
            </TouchableOpacity>
          </View>
        </View>
      </ActionSheet>
    </SafeAreaView>
  );
}

const Styles = StyleSheet.create({
  itemContainer: {
    width: '90%',
    height: 70,
    borderRadius: 10,
    padding: 10,
    margin: 10,
    flexDirection: 'column',
    justifyContent: 'center',
    backgroundColor: stylesheet.SecondaryTint
  }
});
