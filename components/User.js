import React from 'react';
import { View, SafeAreaView, Text, ScrollView, ActivityIndicator, Alert, TouchableOpacity } from 'react-native';
import { Ionicons, MaterialIcons } from '@expo/vector-icons';
import Localization from '../localization/config';

import API from '../Api';

const stylesheet = require('../Styles');
const styles = stylesheet.Styles;
const formats = require('./Formats').Formats;

export default function User({navigation, route}) {
  const SESSION = route.params.session;

  const {name, identifier} = route.params;
  const [isLoading, setIsLoading] = React.useState(true);
  const [wasDeleted, setWasDeleted] = React.useState(false);
  const [lastUpdated, setLastUpdated] = React.useState('');
  const [phoneNumber, setPhoneNumber] = React.useState('');
  const [dob, setDob] = React.useState('');
  const [brand, setBrand] = React.useState({
    identifier: '',
    name: ''
  });
  const [canDelete, setCanDelete] = React.useState(false);

  const onDeleteApprove = async () => {
    const body = {auth: SESSION.crypto, id: identifier};

    try {
      const res = await API.post('user/delete', body);
      navigation.goBack();
    } catch (e) {

    }
  };
  const onDelete = () => {
    Alert.alert(
      'Delete User',
      'Are you sure you want to delete this user?',
      [
        {
          text: 'No',
          onPress: () => {},
          style: 'cancel'
        },
        { text: 'Yes', onPress: () => onDeleteApprove()}
      ],
      { cancelable: true }
    );
  };

  React.useEffect(() => {
    API.get('user', {auth: SESSION.crypto, id: identifier}).then((res) => {
      if (res.isError) throw 'error';

      setWasDeleted(res.data._wd);
      setLastUpdated(res.data._lu);
      setPhoneNumber(res.data._pn);
      setDob(res.data._dob);
      setBrand({
        identifier: res.data._b.identifier,
        name: res.data._b.name
      });
      setCanDelete(res.data._cd);
      setIsLoading(false);
    }).catch(e => {
      setIsLoading(true);
    });
  }, [identifier]);

  return (
    <SafeAreaView style={styles.defaultTabContainer}>
      <View style={styles.defaultTabHeader}>
        <TouchableOpacity
          style={styles.backArrow}
          onPress={() => navigation.goBack()}
          underlayColor='#fff'>
          <Ionicons name="ios-arrow-back" size={24} color={stylesheet.Tertiary} />
        </TouchableOpacity>
        <View style={styles.spacer}></View>
        <Text style={[styles.baseText, styles.bold, styles.tertiary]}>{name}</Text>
        <View style={styles.spacer}></View>
        {
          canDelete &&
          <TouchableOpacity
            onPress={() => onDelete()}
            underlayColor='#fff'>
            <Ionicons name="md-trash" size={24} color={stylesheet.Primary} />
          </TouchableOpacity>
        }
      </View>
      {
        isLoading &&
        <View style={[styles.defaultLoader, styles.fullWidth, styles.fullHeight, styles.defaultColumnContainer]}>
          <View style={{flex: 0.1}}></View>
          <ActivityIndicator size="small" color={stylesheet.Primary} />
        </View>
      }
      <ScrollView style={styles.defaultTabScrollContent} contentContainerStyle={{display: 'flex', minHeight: '100%', alignItems: 'flex-start', justifyContent: 'flex-start', width: '90%', marginLeft: '5%'}}>
        {
          wasDeleted &&
          <>
            <View style={{flex: 0.1}}></View>
            <Text style={[styles.subHeaderText, styles.centerText, styles.fullWidth, styles.primary]}>{Localization.t('deletedAccount')}</Text>
          </>
        }
        <View style={{flex: 0.1}}></View>
        <Text style={[styles.subHeaderText, styles.tertiary]}>{Localization.t('details')}</Text>
        <View style={{flex: 0.05}}></View>
        <Text style={[styles.baseText, styles.tertiary]}>{lastUpdated}</Text>
        <View style={{flex: 0.02}}></View>
        <Text style={[styles.tinyText, styles.opaque, styles.tertiary]}>{Localization.t('lastUpdated')}</Text>
        <View style={{flex: 0.08}}></View>
        <Text selectable={true} style={[styles.baseText, styles.tertiary]}>{identifier}</Text>
        <View style={{flex: 0.02}}></View>
        <Text style={[styles.tinyText, styles.opaque, styles.tertiary]}>{Localization.t('id')}</Text>
        <View style={{flex: 0.08}}></View>
        <Text style={[styles.baseText, styles.tertiary]}>{phoneNumber}</Text>
        <View style={{flex: 0.02}}></View>
        <Text style={[styles.tinyText, styles.opaque, styles.tertiary]}>{Localization.t('phoneNumber')}</Text>
        <View style={{flex: 0.08}}></View>
        <Text style={[styles.baseText, styles.tertiary]}>{dob}</Text>
        <View style={{flex: 0.02}}></View>
        <Text style={[styles.tinyText, styles.opaque, styles.tertiary]}>{Localization.t('DoB')}</Text>
        {
          brand.identifier !== '' &&
          <>
            <View style={{flex: 0.1}}></View>
            <Text onPress={() => navigation.navigate('Brand', {session: SESSION, identifier: brand.identifier, name: brand.name})} style={[styles.baseText, styles.primary]}>{brand.name}</Text>
            <View style={{flex: 0.02}}></View>
            <Text style={[styles.tinyText, styles.opaque, styles.tertiary]}>{Localization.t('brand')}</Text>
          </>
        }
      </ScrollView>
    </SafeAreaView>
  );
}
