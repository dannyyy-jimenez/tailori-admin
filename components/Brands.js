import React from 'react';
import { View, SafeAreaView, Text, StyleSheet, ScrollView, ActivityIndicator, RefreshControl, Image, TextInput, TouchableOpacity } from 'react-native';
import { Ionicons, MaterialIcons } from '@expo/vector-icons';
import ActionSheet from "react-native-actions-sheet";
import Localization from '../localization/config';

import API from '../Api';
import Brand from '../models/Brand';

const stylesheet = require('../Styles');
const styles = stylesheet.Styles;
const formats = require('./Formats').Formats;

const actionSheetRef = React.createRef();

export default function Brands({navigation, route}) {
  const SESSION = route.params.session;

  const [searchQuery, setSearchQuery] = React.useState('');
  const [refreshing, setRefreshing] = React.useState(false);
  const [isLoading, setIsLoading] = React.useState(true);
  const [brands, setBrands] = React.useState([]);
  const [baseBrands, setBaseBrands] = React.useState([]);
  const [hasPermission, setHasPermission] = React.useState(false);
  const [sortBy, setSortBy] = React.useState('newest');
  const [hasReleases, setHasReleases] = React.useState(true);

  const load = async () => {
    setIsLoading(true);

    try {
      const res =  await API.get('brands', {auth: SESSION.crypto, sortBy, hasReleases});
      if (res.isError && res.response === 'NO_PERMISSION') throw 'permission';

      if (res.isError) throw 'error';

      setHasPermission(true);
      setBaseBrands(res.data._b.map(brand => new Brand(brand.id, brand.identifier, brand.name, brand.logo, brand.payouts, brand.distribution, brand.hasReleases)));
      setBrands(res.data._b.map(brand => new Brand(brand.id, brand.identifier, brand.name, brand.logo, brand.payouts, brand.distribution, brand.hasReleases)));
      setIsLoading(false);
    } catch (e) {
      if (e === 'permission') {
        setHasPermission(false);
      }
      setIsLoading(false);
    };
  };

  const refresh = async() => {
    setRefreshing(true);
    await load();
    setRefreshing(false);
  }

  React.useEffect(() => {
    load();
  }, [sortBy, hasReleases]);

  React.useEffect(() => {
    if (searchQuery.replace(/ /g, '') === '') {
      return;
      setBrands(baseBrands.slice());
    }
    setBrands(baseBrands.filter((brand) => {
      let likeRegex = new RegExp(searchQuery, 'gi');
      return likeRegex.test(brand.getIdentifier()) || likeRegex.test(brand.getName()) || likeRegex.test(brand.getID());
    }));
  }, [searchQuery]);

  return (
    <SafeAreaView style={styles.defaultTabContainer}>
      <View style={styles.defaultTabHeader}>
        <TouchableOpacity
          onPress={() => actionSheetRef.current?.setModalVisible(true)}
          underlayColor='#fff'>
          <MaterialIcons name="sort" size={24} color={stylesheet.Primary} />
        </TouchableOpacity>
        <View style={styles.spacer}></View>
        <TextInput style={[styles.baseInput, styles.filledInput, styles.tertiary]} value={searchQuery} onChangeText={value => setSearchQuery(value)} keyboardType="default" placeholder={Localization.t('searchBrands')} placeholderTextColor="#555555" returnKeyType="search" selectionColor="#DB113B" textContentType="none" />
        <View style={styles.spacer}></View>
      </View>
      {
        isLoading &&
        <View style={[styles.defaultLoader, styles.fullWidth, styles.fullHeight, styles.defaultColumnContainer]}>
          <View style={{flex: 0.1}}></View>
          <ActivityIndicator size="small" color={stylesheet.Primary} />
        </View>
      }
      <ScrollView style={styles.defaultTabScrollContent} contentContainerStyle={{alignItems: 'center', justifyContent: 'flex-start', width: '90%', marginLeft: '5%'}} refreshControl={<RefreshControl refreshing={refreshing} tintColor={stylesheet.Primary} colors={[stylesheet.Primary]} onRefresh={load} />}>
        <View style={{flex: 0.05}}></View>
        {
          !hasPermission &&
          <>
            <View style={styles.spacer}></View>
            <Ionicons name="md-lock" size={48} color={stylesheet.Primary} />
            <View style={{flex: 0.05}}></View>
            <Text style={[styles.centerText, styles.primary, styles.bold, styles.baseText]}>{Localization.t('NO_PERMISSION')}</Text>
            <View style={styles.spacer}></View>
          </>
        }
        {
          !isLoading && brands.map((brand) => {
            return (
              <TouchableOpacity key={brand.getIdentifier()} onPress={() => navigation.navigate('Brand', {session: SESSION, identifier: brand.getIdentifier(), name: brand.getName()})} style={[Styles.itemContainer, brand.hasReleases ? {opacity: 1} : styles.opaque]}>
                <Image style={Styles.itemImage} source={{uri: formats.cloudinarize(brand.getLogo(), 'c_scale,h_208/'), cache: 'force-cache'}}/>
                <View style={{flex: 0.02}}></View>
                <View style={[styles.defaultColumnContainer, styles.spacer]}>
                  <View style={styles.defaultRowContainer}>
                    <Text style={[styles.baseText, styles.bold, styles.tertiary]}>{brand.getName()}</Text>
                    <View style={styles.spacer}></View>
                    <Text style={[styles.tinyText, styles.opaque, styles.tertiary]}>${formats.abbreviate(brand.payouts, true)}</Text>
                  </View>
                  <View style={styles.spacer}></View>
                  <View style={styles.defaultRowContainer}>
                    <Text style={[styles.tinyText, styles.opaque, styles.bold, styles.tertiary]}>{brand.getIdentifier()}</Text>
                    <View style={styles.spacer}></View>
                    <Text style={[styles.tinyText, styles.opaque, styles.tertiary]}>{formats.abbreviate(brand.distribution)}</Text>
                  </View>
                </View>
              </TouchableOpacity>
            )
          })
        }
      </ScrollView>
      <ActionSheet containerStyle={{paddingBottom: 20, backgroundColor: stylesheet.Secondary}} indicatorColor={stylesheet.Primary} gestureEnabled={true} ref={actionSheetRef}>
        <View>
          <Text style={[styles.baseText, styles.fullWidth, styles.bold, styles.centerText, styles.tertiary, {marginTop: 10}]}>{Localization.t('filters')}</Text>
          <View style={styles.line}></View>
          <Text style={[styles.baseText, styles.marginWidth, styles.bold, styles.tertiary, {marginTop: 10, marginBottom: 10}]}>{Localization.t('sortBy')}</Text>
          <View style={[styles.paddedWidth, styles.defaultColumnContainer]}>
            <TouchableOpacity onPress={() => setSortBy('newest')} disabled={sortBy === 'newest'} style={[styles.defaultRowContainer, styles.actionListItem, sortBy === 'newest' ? styles.disabled : {}]}>
              <Text style={[styles.spacer, styles.baseText, styles.tertiary]}>{Localization.t('newest')}</Text>
              {
                sortBy === 'newest' &&
                <Ionicons name="md-checkmark" size={18} color={stylesheet.Secondary} />
              }
            </TouchableOpacity>
          </View>
          <View style={[styles.paddedWidth, styles.defaultColumnContainer]}>
            <TouchableOpacity onPress={() => setSortBy('oldest')} disabled={sortBy === 'oldest'} style={[styles.defaultRowContainer, styles.actionListItem, sortBy === 'oldest' ? styles.disabled : {}]}>
              <Text style={[styles.spacer, styles.baseText, styles.tertiary]}>{Localization.t('oldest')}</Text>
              {
                sortBy === 'oldest' &&
                <Ionicons name="md-checkmark" size={18} color={stylesheet.Secondary} />
              }
            </TouchableOpacity>
          </View>

          <Text style={[styles.baseText, styles.marginWidth, styles.bold, styles.tertiary, {marginTop: 20, marginBottom: 10}]}>{Localization.t('hasReleases')}</Text>
          <View style={[styles.paddedWidth, styles.defaultColumnContainer]}>
            <TouchableOpacity onPress={() => setHasReleases(true)} disabled={hasReleases} style={[styles.defaultRowContainer, styles.actionListItem, hasReleases ? styles.disabled : {}]}>
              <Text style={[styles.spacer, styles.baseText, styles.tertiary]}>{Localization.t('true')}</Text>
              {
                hasReleases &&
                <Ionicons name="md-checkmark" size={18} color={stylesheet.Secondary} />
              }
            </TouchableOpacity>
          </View>
          <View style={[styles.paddedWidth, styles.defaultColumnContainer]}>
            <TouchableOpacity onPress={() => setHasReleases(false)} disabled={!hasReleases} style={[styles.defaultRowContainer, styles.actionListItem, !hasReleases ? styles.disabled : {}]}>
              <Text style={[styles.spacer, styles.baseText, styles.tertiary]}>{Localization.t('false')}</Text>
              {
                !hasReleases &&
                <Ionicons name="md-checkmark" size={18} color={stylesheet.Secondary} />
              }
            </TouchableOpacity>
          </View>
        </View>
      </ActionSheet>
    </SafeAreaView>
  );
}

const Styles = StyleSheet.create({
  itemContainer: {
    width: 300,
    height: 200,
    borderRadius: 10,
    padding: 10,
    margin: 10,
    flexDirection: 'column',
    justifyContent: 'center',
    backgroundColor: stylesheet.SecondaryTint
  },
  itemImage: {
    height: 145,
    width: 'auto',
    resizeMode: 'contain'
  }
});
