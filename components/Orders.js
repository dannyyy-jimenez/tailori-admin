import React from 'react';
import { View, SafeAreaView, Text, StyleSheet, RefreshControl, TextInput, ScrollView, ActivityIndicator, TouchableOpacity } from 'react-native';
import { Ionicons, MaterialIcons, MaterialCommunityIcons } from '@expo/vector-icons';
import ActionSheet from "react-native-actions-sheet";
import Localization from '../localization/config';

import API from '../Api';
import Order from '../models/Order';

const stylesheet = require('../Styles');
const styles = stylesheet.Styles;
const formats = require('./Formats').Formats;

const actionSheetRef = React.createRef();

export default function Orders({navigation, route}) {
  const SESSION = route.params.session;

  const [searchQuery, setSearchQuery] = React.useState('');
  const [refreshing, setRefreshing] = React.useState(false);
  const [isLoading, setIsLoading] = React.useState(true);
  const [orders, setOrders] = React.useState([]);
  const [baseOrders, setBaseOrders] = React.useState([]);
  const [hasPermission, setHasPermission] = React.useState(false);
  const [sortBy, setSortBy] = React.useState('newest');
  const [displayFulfilled, setDisplayFulfilled] = React.useState(false);

  const load = async () => {
    setIsLoading(true);

    try {
      const res =  await API.get('orders', {auth: SESSION.crypto, sortBy, displayFulfilled});
      if (res.isError && res.response === 'NO_PERMISSION') throw 'permission';

      if (res.isError) throw 'error';

      setHasPermission(true);
      setBaseOrders(res.data._o.map(order => new Order(order.identifier, order.buyer, order.items, order.total, order.date, order.daysLeft, order.fulfilled)));
      setOrders(res.data._o.map(order => new Order(order.identifier, order.buyer, order.items, order.total, order.date, order.daysLeft, order.fulfilled)));
      setIsLoading(false);
    } catch (e) {
      if (e === 'permission') {
        setHasPermission(false);
      }
      setIsLoading(false);
    };
  };

  const refresh = async() => {
    setRefreshing(true);
    await load();
    setRefreshing(false);
  }

  React.useEffect(() => {
    load();
  }, [sortBy, displayFulfilled]);

  React.useEffect(() => {
    if (searchQuery.replace(/ /g, '') === '') {
      return;
      setOrders(baseOrders.slice());
    }
    setOrders(baseOrders.filter((order) => {
      let likeRegex = new RegExp(searchQuery, 'gi');
      return likeRegex.test(order.getIdentifier()) || likeRegex.test(order.getBuyer());
    }));
  }, [searchQuery]);

  return (
    <SafeAreaView style={styles.defaultTabContainer}>
      <View style={styles.defaultTabHeader}>
        <TouchableOpacity
          onPress={() => actionSheetRef.current?.setModalVisible(true)}
          underlayColor='#fff'>
          <MaterialIcons name="sort" size={24} color={stylesheet.Primary} />
        </TouchableOpacity>
        <View style={styles.spacer}></View>
        <TextInput style={[styles.baseInput, styles.filledInput, styles.tertiary]} value={searchQuery} onChangeText={value => setSearchQuery(value)} keyboardType="default" placeholder={Localization.t('searchOrders')} placeholderTextColor="#555555" returnKeyType="search" selectionColor="#DB113B" textContentType="none" />
        <View style={styles.spacer}></View>
      </View>
      {
        isLoading &&
        <View style={[styles.defaultLoader, styles.fullWidth, styles.fullHeight, styles.defaultColumnContainer]}>
          <View style={{flex: 0.1}}></View>
          <ActivityIndicator size="small" color={stylesheet.Primary} />
        </View>
      }
      <ScrollView style={styles.defaultTabScrollContent} contentContainerStyle={{alignItems: 'center', justifyContent: 'flex-start', width: '90%', marginLeft: '5%'}} refreshControl={<RefreshControl refreshing={refreshing} tintColor={stylesheet.Primary} colors={[stylesheet.Primary]} onRefresh={load} />}>
        <View style={{flex: 0.05}}></View>
        {
          !hasPermission &&
          <>
            <View style={styles.spacer}></View>
            <Ionicons name="md-lock" size={48} color={stylesheet.Primary} />
            <View style={{flex: 0.05}}></View>
            <Text style={[styles.centerText, styles.primary, styles.bold, styles.baseText]}>{Localization.t('NO_PERMISSION')}</Text>
            <View style={styles.spacer}></View>
          </>
        }
        {
          orders.map((order) => {
            return (
              <TouchableOpacity key={order.getIdentifier()} onPress={() => navigation.navigate('Order', {session: SESSION, identifier: order.getIdentifier()})} style={[Styles.itemContainer, !order.wasFulfilled() ? {opacity: 1} : {opacity: 0.2}]}>
                {
                  order.getDaysLeft() < 3 && order.daysLeft > 1 &&
                  <View style={[styles.defaultRowContainer, {marginBottom: 10}]}>
                    <Text style={[styles.baseText, styles.fullWidth, styles.centerText, styles.tertiary]}>{order.getDaysLeftFormatted()}</Text>
                  </View>
                }
                {
                  order.getDaysLeft() <= 1 &&
                  <View style={[styles.defaultRowContainer, {marginBottom: 10}]}>
                    <Text style={[styles.baseText, styles.fullWidth, styles.bold, styles.centerText, styles.primary]}>{order.getDaysLeftFormatted()}</Text>
                  </View>
                }
                <View style={styles.defaultRowContainer}>
                  <Text style={[styles.baseText, styles.bold, styles.tertiary]}>{order.getItemsFormat()}</Text>
                  <View style={styles.spacer}></View>
                  <Text style={[styles.tinyText, styles.opaque, styles.tertiary]}>${order.getTotal()}</Text>
                </View>
                <View style={styles.spacer}></View>
                <View style={[styles.defaultRowContainer, {marginTop: 10}]}>
                  <Text style={[styles.tinyText, styles.bold, styles.opaque, styles.tertiary]}>#{order.getIdentifier()}</Text>
                  <View style={styles.spacer}></View>
                  <Text style={[styles.tinyText, styles.opaque, styles.tertiary]}>{order.getDate()}</Text>
                </View>
              </TouchableOpacity>
            )
          })
        }
      </ScrollView>
      <ActionSheet containerStyle={{paddingBottom: 20, backgroundColor: stylesheet.Secondary}} indicatorColor={stylesheet.Primary} gestureEnabled={true} ref={actionSheetRef}>
        <View>
          <Text style={[styles.baseText, styles.fullWidth, styles.bold, styles.centerText, styles.tertiary, {marginTop: 10}]}>{Localization.t('filters')}</Text>
          <View style={styles.line}></View>
          <Text style={[styles.baseText, styles.marginWidth, styles.bold, styles.tertiary, {marginTop: 10, marginBottom: 10}]}>{Localization.t('sortBy')}</Text>
          <View style={[styles.paddedWidth, styles.defaultColumnContainer]}>
            <TouchableOpacity onPress={() => setSortBy('newest')} disabled={sortBy === 'newest'} style={[styles.defaultRowContainer, styles.actionListItem, sortBy === 'newest' ? styles.disabled : {}]}>
              <Text style={[styles.spacer, styles.baseText, styles.tertiary]}>{Localization.t('newest')}</Text>
              {
                sortBy === 'newest' &&
                <Ionicons name="md-checkmark" size={18} color={stylesheet.Secondary} />
              }
            </TouchableOpacity>
          </View>
          <View style={[styles.paddedWidth, styles.defaultColumnContainer]}>
            <TouchableOpacity onPress={() => setSortBy('oldest')} disabled={sortBy === 'oldest'} style={[styles.defaultRowContainer, styles.actionListItem, sortBy === 'oldest' ? styles.disabled : {}]}>
              <Text style={[styles.spacer, styles.baseText, styles.tertiary]}>{Localization.t('oldest')}</Text>
              {
                sortBy === 'oldest' &&
                <Ionicons name="md-checkmark" size={18} color={stylesheet.Secondary} />
              }
            </TouchableOpacity>
          </View>

          <Text style={[styles.baseText, styles.marginWidth, styles.bold, styles.tertiary, {marginTop: 20, marginBottom: 10}]}>{Localization.t('displayFulfilled')}</Text>
          <View style={[styles.paddedWidth, styles.defaultColumnContainer]}>
            <TouchableOpacity onPress={() => setDisplayFulfilled(false)} disabled={!displayFulfilled} style={[styles.defaultRowContainer, styles.actionListItem, !displayFulfilled ? styles.disabled : {}]}>
              <Text style={[styles.spacer, styles.baseText, styles.tertiary]}>{Localization.t('no')}</Text>
              {
                !displayFulfilled &&
                <Ionicons name="md-checkmark" size={18} color={stylesheet.Secondary} />
              }
            </TouchableOpacity>
          </View>
          <View style={[styles.paddedWidth, styles.defaultColumnContainer]}>
            <TouchableOpacity onPress={() => setDisplayFulfilled(true)} disabled={displayFulfilled} style={[styles.defaultRowContainer, styles.actionListItem, displayFulfilled ? styles.disabled : {}]}>
              <Text style={[styles.spacer, styles.baseText, styles.tertiary]}>{Localization.t('yes')}</Text>
              {
                displayFulfilled &&
                <Ionicons name="md-checkmark" size={18} color={stylesheet.Secondary} />
              }
            </TouchableOpacity>
          </View>
        </View>
      </ActionSheet>
    </SafeAreaView>
  );
}

const Styles = StyleSheet.create({
  itemContainer: {
    width: '90%',
    minHeight: 70,
    borderRadius: 10,
    padding: 10,
    margin: 10,
    flexDirection: 'column',
    justifyContent: 'center',
    backgroundColor: stylesheet.SecondaryTint
  }
});
