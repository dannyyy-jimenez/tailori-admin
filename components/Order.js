import React from 'react';
import { View, SafeAreaView, Text, Linking, StyleSheet, Image, ScrollView, ActivityIndicator, TouchableOpacity } from 'react-native';
import { Ionicons, MaterialIcons, FontAwesome5 } from '@expo/vector-icons';
import Localization from '../localization/config';

import API from '../Api';

const stylesheet = require('../Styles');
const styles = stylesheet.Styles;
const formats = require('./Formats').Formats;

export default function Order({navigation, route}) {
  const SESSION = route.params.session;

  const identifier = route.params.identifier;
  const [isLoading, setIsLoading] = React.useState(true);
  const [wasFulfilled, setWasFulfilled] = React.useState(false);
  const [placed, setPlaced] = React.useState('');
  const [total, setTotal] = React.useState('');
  const [shipping, setShipping] = React.useState('');
  const [cop, setCoP] = React.useState('');
  const [experimental, setExperimental] = React.useState('');
  const [taxes, setTaxes] = React.useState('');
  const [address, setAddress] = React.useState('');
  const [showReceipt, toggleReceipt] = React.useState(false);
  const [showUpdates, toggleUpdates] = React.useState(false);
  const [buyer, setBuyer] = React.useState({
    identifier: '',
    name: ''
  });
  const [updates, setUpdates] = React.useState([]);
  const [payouts, setPayouts] = React.useState('');
  const [estimatedGains, setEstimatedGains] = React.useState('');
  const [items, setItems] = React.useState([]);

  React.useEffect(() => {
    API.get('order', {auth: SESSION.crypto, identifier: identifier}).then((res) => {
      if (res.isError) throw 'error';

      setWasFulfilled(res.data._f);
      setPlaced(res.data._c);
      setTotal(res.data._t);
      setShipping(res.data._sP);
      setCoP(res.data._cop);
      setExperimental(res.data._exp);
      setAddress(res.data._a);
      setTaxes(res.data._tx);
      setBuyer({
        identifier: res.data._b.identifier,
        name: res.data._b.name
      });
      setUpdates(res.data._u);
      setPayouts(res.data._po);
      setEstimatedGains(res.data._eg);
      setItems(res.data._i);
      setIsLoading(false);
    }).catch(e => {
      setIsLoading(true);
    });
  }, [identifier]);

  const onLinkTap = async (url) => {
    const link = formats.cloudinarize(url, 'q_100/');

    const supported = await Linking.canOpenURL(link);

    if (supported) {
      await Linking.openURL(link);
    }
  };

  return (
    <SafeAreaView style={styles.defaultTabContainer}>
      <View style={styles.defaultTabHeader}>
        <TouchableOpacity
          style={styles.backArrow}
          onPress={() => navigation.goBack()}
          underlayColor='#fff'>
          <Ionicons name="ios-arrow-back" size={24} color={stylesheet.Tertiary} />
        </TouchableOpacity>
        <View style={styles.spacer}></View>
        <Text style={[styles.baseText, styles.bold, styles.tertiary]}>#{identifier}</Text>
        <View style={styles.spacer}></View>
        <TouchableOpacity
          style={styles.backArrow}
          onPress={() => navigation.navigate('Payout', {session: SESSION, identifier: identifier})}
          underlayColor='#fff'>
          <FontAwesome5 name="money-check" size={24} color={stylesheet.Tertiary} />
        </TouchableOpacity>
        <View style={{flex: 0.10}}></View>
        <TouchableOpacity
          style={styles.backArrow}
          onPress={() => navigation.navigate('Update', {session: SESSION, identifier: identifier})}
          underlayColor='#fff'>
          <Ionicons name="md-paper-plane" size={24} color={stylesheet.Tertiary} />
        </TouchableOpacity>
      </View>
      {
        isLoading &&
        <View style={[styles.defaultLoader, styles.fullWidth, styles.fullHeight, styles.defaultColumnContainer]}>
          <View style={{flex: 0.1}}></View>
          <ActivityIndicator size="small" color={stylesheet.Primary} />
        </View>
      }
      <ScrollView style={styles.defaultTabScrollContent} contentContainerStyle={{alignItems: 'flex-start', minHeight: '100%', justifyContent: 'flex-start', width: '90%', marginLeft: '5%'}}>
        {
          wasFulfilled &&
          <>
            <View style={{flex: 0.1}}></View>
            <Text style={[styles.subHeaderText, styles.centerText, styles.fullWidth, styles.primary]}>{Localization.t('orderFulfilled')}</Text>
          </>
        }
        <View style={{flex: 0.1}}></View>
        <Text style={[styles.subHeaderText, styles.tertiary, {marginBottom: 20}]}>{Localization.t('details')}</Text>
        <Text style={[styles.baseText, styles.tertiary]}>{placed}</Text>
        <View style={{flex: 0.02}}></View>
        <Text style={[styles.tinyText, styles.opaque, styles.tertiary, {marginBottom: 20}]}>{Localization.t('orderPlaced')}</Text>
        {
          showReceipt &&
          <>
            {
              cop !== '0.00' &&
              <>
                <Text selectable={true} style={[styles.baseText, styles.primary]}>${cop}</Text>
                <View style={{flex: 0.02}}></View>
                <Text style={[styles.tinyText, styles.opaque, styles.tertiary, {marginBottom: 10}]}>{Localization.t('CoP' )}</Text>
              </>
            }
            {
              experimental !== '0.00' &&
              <>
                <Text selectable={true} style={[styles.baseText, styles.primary]}>${experimental}</Text>
                <View style={{flex: 0.02}}></View>
                <Text style={[styles.tinyText, styles.opaque, styles.tertiary, {marginBottom: 10}]}>{Localization.t('expPayouts' )}</Text>
              </>
            }
            {
              payouts !== '0.00' &&
              <>
                <Text selectable={true} style={[styles.baseText, styles.primary]}>${payouts}</Text>
                <View style={{flex: 0.02}}></View>
                <Text style={[styles.tinyText, styles.opaque, styles.tertiary, {marginBottom: 10}]}>{Localization.t('payouts' )}</Text>
              </>
            }
            <Text selectable={true} style={[styles.baseText, styles.primary]}>${taxes}</Text>
            <View style={{flex: 0.02}}></View>
            <Text style={[styles.tinyText, styles.opaque, styles.tertiary, {marginBottom: 10}]}>{Localization.t('taxes')}</Text>
            <Text selectable={true} style={[styles.baseText, styles.primary]}>${shipping}</Text>
            <View style={{flex: 0.02}}></View>
            <Text style={[styles.tinyText, styles.opaque, styles.tertiary, {marginBottom: 10}]}>{Localization.t('shipping')}</Text>
          </>
        }
        <Text onPress={() => toggleReceipt(!showReceipt)} selectable={true} style={[styles.baseText, styles.tertiary]}>${total}</Text>
        <View onPress={() => toggleReceipt(!showReceipt)} style={{flex: 0.02}}></View>
        <Text onPress={() => toggleReceipt(!showReceipt)} style={[styles.tinyText, styles.opaque, styles.tertiary, {marginBottom: 20}]}>{Localization.t('total')}</Text>
        <Text selectable={true} style={[styles.baseText, styles.tertiary]}>${estimatedGains}</Text>
        <View style={{flex: 0.02}}></View>
        <Text style={[styles.tinyText, styles.opaque, styles.tertiary, {marginBottom: showUpdates ? 40 : 20}]}>{Localization.t('estimatedGains')}</Text>
        {
          !showUpdates &&
          <>
            <Text style={[styles.baseText, styles.tertiary]}>{address}</Text>
            <View style={{flex: 0.02}}></View>
            <Text style={[styles.tinyText, styles.opaque, styles.tertiary, {marginBottom: 20}]}>{Localization.t('shipTo')}</Text>
          </>
        }
        {
          !showUpdates &&
          <>
            <Text onPress={() => navigation.navigate('User', {session: SESSION, identifier: buyer.identifier, name: buyer.name})} style={[styles.baseText, styles.primary]}>{buyer.name}</Text>
            <View style={{flex: 0.02}}></View>
            <Text style={[styles.tinyText, styles.opaque, styles.tertiary, {marginBottom: 20}]}>{Localization.t('buyer')}</Text>
          </>
        }
        {
          showUpdates &&
          updates.map((update, index) => {
            return (
              <>
                <Text key={"UPDT_CONTENT_"+index} style={[styles.baseText, styles.tertiary]}>{update.content}</Text>
                <View key={"UPDT_SPC_"+index} style={{flex: 0.02}}></View>
                <Text key={"UPDT_PROVIDER_"+index} style={[styles.tinyText, styles.opaque, styles.tertiary, {marginBottom: 10}]}>{update.date} - { update.provider }</Text>
              </>
            )
          })
        }
        <Text onPress={() => toggleUpdates(!showUpdates)} style={[styles.baseText, styles.tertiary]}>{updates.length} {Localization.t('updates')}</Text>
        <View onPress={() => toggleUpdates(!showUpdates)} style={{flex: 0.02}}></View>
        <Text onPress={() => toggleUpdates(!showUpdates)} style={[styles.tinyText, styles.opaque, styles.tertiary, {marginBottom: showUpdates ? 40 : 20}]}>{showUpdates ? Localization.t('tapToHide') : Localization.t('tapToExpand')}</Text>
        {
          !showUpdates && items.map(design => {
            return (
              <TouchableOpacity key={design.id} onLongPress={() => navigation.navigate('Design', {session: SESSION, identifier: design.identifier, name: design.name})} style={[Styles.itemContainer, design.fulfilled ? {opacity: 0.6} : {opacity: 1}]}>
                {
                  design.fulfilled &&
                  <View style={[Styles.fulfilledItem, styles.fullWidth, styles.fullHeight, styles.defaultColumnContainer, styles.center]}>
                    <Ionicons name="md-checkmark-circle-outline" size={36} color={stylesheet.Primary} />
                  </View>
                }
                <Image style={Styles.itemImage} source={{uri: formats.cloudinarize(design.cover)}}/>
                <View style={{flex: 0.02}}></View>
                <View style={[styles.defaultColumnContainer, styles.spacer]}>
                  <View style={styles.defaultRowContainer}>
                    <Text style={[styles.baseText, styles.bold, styles.tertiary]}>{design.token}</Text>
                    <View style={styles.spacer}></View>
                    <Text style={[styles.tinyText, styles.bold, styles.opaque, styles.primary]}>x {design.quantity}</Text>
                  </View>
                  <View style={styles.spacer}></View>
                  <View style={styles.defaultColumnContainer}>
                    <Text style={[styles.tinyText, styles.opaque, styles.tertiary]}>{Localization.t('color')}: {design.color}</Text>
                    <View style={{flex: 0.4}}></View>
                    <Text style={[styles.tinyText, styles.opaque, styles.tertiary]}>{Localization.t('size')}: {design.size}</Text>
                    {
                      design.metrics.map(metric => {
                        return (
                          <Text key={"METRIC_"+metric.side} style={[styles.tinyText, styles.opaque, styles.tertiary, {marginTop: 15}]}>{formats.formatSide(metric.side)}: <Text onPress={() => onLinkTap(metric.artwork)} style={styles.primary}>{formats.cloudinarize(metric.artwork, 'q_100/')}</Text></Text>
                        )
                      })
                    }
                  </View>
                </View>
              </TouchableOpacity>
            )
          })
        }
      </ScrollView>
    </SafeAreaView>
  );
}

const Styles = StyleSheet.create({
  itemContainer: {
    width: '100%',
    minHeight: 300,
    borderRadius: 20,
    padding: 20,
    marginTop: 10,
    marginBottom: 10,
    flexDirection: 'column',
    backgroundColor: stylesheet.SecondaryTint
  },
  itemImage: {
    height: 200,
    width: 'auto',
    resizeMode: 'contain'
  },
  fulfilledItem: {
    position: 'absolute',
    zIndex: 1000,
    top: 0,
    borderRadius: 20,
    left: 0,
    backgroundColor: stylesheet.SecondaryTint,
    opacity: 0.8
  }
});
