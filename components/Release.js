import React from 'react';
import { View, SafeAreaView, Text, ActivityIndicator, Linking, Dimensions,ScrollView, StyleSheet, Alert, Image, TouchableOpacity } from 'react-native';
import { Ionicons, MaterialIcons, FontAwesome5 } from '@expo/vector-icons';
import Clipboard from 'expo-clipboard';
import Localization from '../localization/config';

import API from '../Api';

const stylesheet = require('../Styles');
const styles = stylesheet.Styles;
const formats = require('./Formats').Formats;

export default function Release({navigation, route}) {
  const SESSION = route.params.session;

  const identifier = route.params.identifier;
  const [isLoading, setIsLoading] = React.useState(true);
  const [created, setCreated] = React.useState('');
  const [shots, setShots] = React.useState([]);
  const [currentShot, setCurrentShot] = React.useState('');
  const [metrics, setMetrics] = React.useState({});
  const [raws, setRaws] = React.useState([]);
  const [type, setType] = React.useState('');
  const [mode, setMode] = React.useState('');
  const [description, setDescription] = React.useState('');
  const [showMetrics, setShowMetrics] = React.useState(false);
  const [designer, setDesigner] = React.useState({
    identifier: '',
    name: ''
  });
  const [design, setDesign] = React.useState({
    identifier: '',
    name: ''
  });
  const [wasDenied, setWasDenied] = React.useState(false);
  const [wasApproved, setWasApproved] = React.useState(false);
  const [canApprove, setCanApprove] = React.useState(false);
  const [canValidate, setCanValidate] = React.useState(false);
  const [price, setPrice] = React.useState('');
  const [royalty, setRoyalty] = React.useState('');
  const [cop, setCoP] = React.useState('');
  const [printableError, setPError] = React.useState(false);

  const onReleaseVerdict = async (decision) => {
    setIsLoading(true);
    const body = {auth: SESSION.crypto, identifier, decision: decision};
    try {
      const res = await API.post('release/approve', body);

      if (res.response === 'TRINUM_ERROR') {
        setPError(true);
        throw 'TRINUM_ERROR';
      }

      setPError(false);
      if (decision) {
        setWasApproved(true);
      } else {
        setWasDenied(true);
      }
      setIsLoading(false);
    } catch (e) {
      setIsLoading(false);
    }
  };

  const onShare = async () => {
    Clipboard.setString(`https://www.tailorii.app/shop/zoom/${identifier}`);
    alert('Copied Link!');
  }

  const onBack = () => {
    navigation.goBack();
  };

  const onApprove = () => {
    Alert.alert(
      Localization.t('releaseDecision'),
      Localization.t('releaseVerdict'),
      [
        {
          text: Localization.t('deny'),
          onPress: () => onReleaseVerdict(false),
          style: 'destructive'
        },
        { text: Localization.t('approve'), onPress: () => onReleaseVerdict(true)}
      ],
      { cancelable: true }
    );
  };

  const onThumbsDown = () => {
    Alert.alert(
      'Release Decision',
      'Are you sure you want to disregard this release',
      [
        { text: 'No', onPress: () => null},
        {
          text: 'Yes',
          onPress: () => onValidate({price: 0, decision: false}),
          style: 'destructive'
        }
      ],
      { cancelable: true }
    );
  }

  const onLinkTap = async (url) => {
    const link = formats.cloudinarize(url);
    const supported = await Linking.canOpenURL(link);

    if (supported) {
      await Linking.openURL(link);
    }
  };

  const onValidate = async (data) => {
    setIsLoading(true);
    const body = {auth: SESSION.crypto, identifier, price: data.price, decision: data.decision};
    try {
      const res = await API.post('release/validate', body);

      if (res.response === 'TRINUM_ERROR') {
        setPError(true);
        throw 'TRINUM_ERROR';
      }

      setCoP(res.data._cop);
      setPrice(res.data._price);
      setCanValidate(false);
      setIsLoading(false);
    } catch (e) {
      setIsLoading(false);
    }
  };

  const getShotIndex = (currentShot) => {
    if (shots.length === 1) return 0;

    return shots.indexOf(currentShot);
  }

  const getNextShot = (currentShot, number = false) => {
    if (shots.indexOf(currentShot) === shots.length - 1) {
      if (number) return 0;
      return shots[0];
    }

    if (number) return shots.indexOf(currentShot) + 1;
    return shots[shots.indexOf(currentShot) + 1];
  }

  React.useEffect(() => {
    setIsLoading(true);

    API.get('release', {auth: SESSION.crypto, identifier}).then((res) => {
      if (res.isError) throw 'error';

      setWasApproved(res.data._v);
      setCreated(res.data._cA);
      setDesigner({
        identifier: res.data._ds.identifier,
        name: res.data._ds.name
      });
      setDesign({
        identifier: res.data._dID,
        name: res.data._n
      });
      setMetrics(res.data._mtr);
      setRaws(res.data._rws);
      setShots(res.data._s);
      setCurrentShot(res.data._s[0]);
      setType(res.data._t);
      setMode(res.data._m);
      setCoP(res.data._cop);
      setRoyalty(res.data._r);
      setPrice(res.data._p);
      setDescription(res.data._d);
      setCanApprove(res.data._hP);
      setCanValidate(res.data._hV);

      if (!res.data._v && res.data._hV) {
        setShowMetrics(true);
      }
      setIsLoading(false);
    }).catch(e => {
      navigation.goBack();
    });
  }, [identifier]);

  return (
    <SafeAreaView style={styles.defaultTabContainer}>
      <View style={styles.defaultTabHeader}>
        <TouchableOpacity
          style={styles.backArrow}
          onPress={() => onBack()}
          underlayColor='#fff'>
          <Ionicons name="ios-arrow-back" size={24} color={stylesheet.Tertiary} />
        </TouchableOpacity>
        <View style={styles.spacer}></View>
        <Text style={[styles.baseText, styles.bold, styles.tertiary]}>#{identifier}</Text>
        <View style={styles.spacer}></View>
        {
          !isLoading && wasApproved &&
          <TouchableOpacity
            onPress={() => onShare()}
            underlayColor='#fff'>
            <FontAwesome5 name="share" size={24} color={stylesheet.Tertiary} />
          </TouchableOpacity>
        }
        {
          canApprove && !isLoading && !wasApproved &&
          <TouchableOpacity
            onPress={() => onApprove()}
            underlayColor='#fff'>
            <Ionicons name="md-checkmark-circle-outline" size={24} color={stylesheet.Tertiary} />
          </TouchableOpacity>
        }
        {
          canValidate && !isLoading && !wasApproved &&
          <TouchableOpacity
            onPress={() => navigation.navigate('Validate', {onValidate: (data) => onValidate(data)})}
            underlayColor='#fff'>
            <Ionicons name="md-checkmark-circle-outline" size={24} color={stylesheet.Tertiary} />
          </TouchableOpacity>
        }
        {
          canValidate && !isLoading && !wasApproved &&
          <TouchableOpacity
            style={{marginLeft: 12, marginRight: 12}}
            onPress={() => onThumbsDown()}
            underlayColor='#fff'>
            <Ionicons name="ios-thumbs-down" size={24} color={stylesheet.Tertiary} />
          </TouchableOpacity>
        }
        {
          !isLoading &&
          <TouchableOpacity
            style={{marginLeft: 12, marginRight: 12}}
            onPress={() => setShowMetrics(!showMetrics)}
            underlayColor='#fff'>
            <Ionicons name="images" size={24} color={stylesheet.Tertiary} />
          </TouchableOpacity>
        }
      </View>
      {
        isLoading &&
        <View style={[styles.defaultLoader, styles.fullWidth, styles.fullHeight, styles.defaultColumnContainer]}>
          <View style={{flex: 0.1}}></View>
          <ActivityIndicator size="small" color={stylesheet.Primary} />
        </View>
      }
      <ScrollView style={styles.defaultTabScrollContent} contentContainerStyle={{alignItems: 'flex-start', minHeight: '100%', justifyContent: 'flex-start', width: '90%', marginLeft: '5%'}}>
        {
          wasApproved &&
          <>
            <View style={{flex: 0.1}}></View>
            <Text style={[styles.subHeaderText, styles.centerText, styles.fullWidth, styles.primary, {marginBottom: 20}]}>{Localization.t('releaseApproved')}</Text>
          </>
        }
        {
          wasDenied &&
          <>
            <View style={{flex: 0.1}}></View>
            <Text style={[styles.subHeaderText, styles.centerText, styles.fullWidth, styles.primary, {marginBottom: 20}]}>{Localization.t('releaseDenied')}</Text>
          </>
        }
        {
          printableError &&
          <>
            <View style={{flex: 0.1}}></View>
            <Text style={[styles.subHeaderText, styles.centerText, styles.fullWidth, styles.primary]}>{Localization.t('TRINUM_ERROR')}</Text>
          </>
        }
        {
          shots.length == 1 &&
          <Image
                style={styles.defaultZoomImage}
                source={{ uri: formats.cloudinarize(shots[0])}}
              />
        }
        {
          shots.length > 1 &&
          <TouchableOpacity style={styles.defaultZoomImageContainer} onPress={() => setCurrentShot(getNextShot(currentShot))}>
              <Image
                style={styles.defaultZoomMultiImage}
                source={{ uri: formats.cloudinarize(currentShot)}}
              />
              <View style={[{position: 'absolute', left: '25%', bottom: 10, backgroundColor: stylesheet.SecondaryTint, width: '50%', height: 3, borderRadius: 3}]}>
                <View style={{position: 'absolute', width: `${100 / shots.length}%`, left: `${(100 / shots.length) * getShotIndex(currentShot)}%`, borderRadius: 3, backgroundColor: stylesheet.Primary, height: '100%'}}></View>
              </View>
            </TouchableOpacity>
        }
        <View style={{flex: 0.05}}></View>
        <Text style={[styles.subHeaderText, styles.tertiary]}>{Localization.t('details')}</Text>
        <Text style={[styles.baseText, styles.tertiary, {marginTop: 10}]}>{created}</Text>
        <Text style={[styles.tinyText, styles.opaque, styles.tertiary]}>{Localization.t('created')}</Text>
        <Text selectable={true} style={[styles.baseText, styles.tertiary, {marginTop: 10}]}>{description}</Text>
        <Text style={[styles.tinyText, styles.opaque, styles.tertiary]}>{Localization.t('description')}</Text>
        {
          cop !== '0.00' &&
          <>
            <Text style={[styles.baseText, styles.tertiary, {marginTop: 10}]}>{cop}</Text>
            <View style={{flex: 0.02}}></View>
            <Text style={[styles.tinyText, styles.opaque, styles.tertiary]}>{Localization.t('CoP')}</Text>
            <View style={{flex: 0.08}}></View>
          </>
        }
        {
          !showMetrics &&
          <>
            <Text selectable={true} style={[styles.baseText, styles.tertiary, {marginTop: 10}]}>{royalty}</Text>
            <View style={{flex: 0.02}}></View>
            <Text style={[styles.tinyText, styles.opaque, styles.tertiary]}>{Localization.t('royalty')}</Text>
            <Text selectable={true} style={[styles.baseText, styles.tertiary, {marginTop: 10}]}>{price}</Text>
            <View style={{flex: 0.02}}></View>
            <Text style={[styles.tinyText, styles.opaque, styles.tertiary]}>{Localization.t('price')}</Text>
            <Text style={[styles.baseText, styles.tertiary, {marginTop: 10}]}>{formats.getType(type)}</Text>
            <View style={{flex: 0.02}}></View>
            <Text style={[styles.tinyText, styles.opaque, styles.tertiary]}>{Localization.t('type')}</Text>
            <Text style={[styles.baseText, styles.tertiary, {marginTop: 10}]}>{formats.getMode(mode)}</Text>
            <View style={{flex: 0.02}}></View>
            <Text style={[styles.tinyText, styles.opaque, styles.tertiary]}>{Localization.t('mode')}</Text>
            <View style={{flex: 0.1}}></View>
            <Text onPress={() => navigation.navigate('Design', {session: SESSION, identifier: design.identifier, name: design.name})} style={[styles.baseText, styles.primary, {marginTop: 10}]}>{design.name}</Text>
            <View style={{flex: 0.02}}></View>
            <Text style={[styles.tinyText, styles.opaque, styles.tertiary]}>{Localization.t('design')}</Text>
            {
              designer.identifier !== '' &&
              <>
                <View style={{flex: 0.1}}></View>
                <Text onPress={() => navigation.navigate('Brand', {session: SESSION, identifier: designer.identifier, name: designer.name})} style={[styles.baseText, styles.primary, {marginTop: 10}]}>{designer.name}</Text>
                <View style={{flex: 0.02}}></View>
                <Text style={[styles.tinyText, styles.opaque, styles.tertiary]}>{Localization.t('brand')}</Text>
              </>
            }
          </>
        }
        {
          showMetrics &&
          <>
            {
              metrics.map((metric) => {
                return (
                  <>
                    <Text selectable={true} onPress={() => onLinkTap(metric.artwork)} style={[styles.baseText, styles.primary]}>{formats.cloudinarize(metric.artwork)}</Text>
                    <View style={{flex: 0.02}}></View>
                    <Text style={[styles.tinyText, styles.opaque, styles.tertiary, {marginBottom: 20}]}>{formats.formatSide(metric.side)}</Text>
                  </>
                )
              })
            }
          </>
        }
        {
          showMetrics && raws.length > 0 &&
          <>
            <Text style={[styles.baseText, styles.tertiary]}>{Localization.t('raws')}</Text>
            {
              raws.map(raw => {
                return (
                  <Text selectable={true} onPress={() => onLinkTap(raw)} style={[styles.baseText, styles.primary]}>{raw}</Text>
                )
              })
            }
          </>
        }
      </ScrollView>
    </SafeAreaView>
  );
}

const Styles = StyleSheet.create({
  singleImage: {
    width: '100%',
    height: 200,
    resizeMode: 'contain'
  },
  itemContainer: {
    width: Dimensions.get('window').width * 0.75,
    marginLeft: Dimensions.get('window').width * 0.025,
    marginRight: Dimensions.get('window').width * 0.025,
    height: 200,
    borderRadius: 30,
    flexDirection: 'column',
    backgroundColor: stylesheet.SecondaryTint
  }
});
