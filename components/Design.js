import React from 'react';
import { View, SafeAreaView, Text, Alert, StyleSheet, Image, Dimensions, ScrollView, ActivityIndicator, TouchableOpacity } from 'react-native';
import { Ionicons, MaterialIcons } from '@expo/vector-icons';
import * as MediaLibrary from 'expo-media-library';
import * as Permissions from 'expo-permissions';
import Localization from '../localization/config';

import API from '../Api';

const stylesheet = require('../Styles');
const styles = stylesheet.Styles;
const formats = require('./Formats').Formats;

export default function Design({navigation, route}) {
  const SESSION = route.params.session;

  const {name, identifier} = route.params;
  const [isLoading, setIsLoading] = React.useState(true);
  const [wasDeleted, setWasDeleted] = React.useState(false);
  const [created, setCreated] = React.useState('');
  const [shots, setShots] = React.useState([]);
  const [currentShot, setCurrentShot] = React.useState('');
  const [type, setType] = React.useState('');
  const [mode, setMode] = React.useState('');
  const [description, setDescription] = React.useState('');
  const [designer, setDesigner] = React.useState({
    identifier: '',
    name: ''
  });
  const [owner, setOwner] = React.useState({
    identifier: '',
    name: ''
  });

  const askForCameraRollAsync = async () => {
    await Permissions.askAsync(Permissions.CAMERA_ROLL);
  }

  const saveImage = () => {
    for (let shot of shots) {
      MediaLibrary.saveToLibraryAsync(formats.cloudinarize(shot, 'b_rgb:DB113B/') + '.png');
    }
    Alert.alert(
      "Design Shots Saved!",
      '',
      [
        { text: "Cool!", onPress: () => {} }
      ],
      { cancelable: true }
    );
  };

  const getShotIndex = (currentShot) => {
    if (shots.length === 1) return 0;

    return shots.indexOf(currentShot);
  }

  const getNextShot = (currentShot, number = false) => {
    if (shots.indexOf(currentShot) === shots.length - 1) {
      if (number) return 0;
      return shots[0];
    }

    if (number) return shots.indexOf(currentShot) + 1;
    return shots[shots.indexOf(currentShot) + 1];
  }

  React.useEffect(() => {
    askForCameraRollAsync();
  }, []);

  React.useEffect(() => {
    API.get('design', {auth: SESSION.crypto, id: identifier}).then((res) => {
      if (res.isError) throw 'error';

      setWasDeleted(res.data._wd);
      setCreated(res.data._cA);
      setDesigner({
        identifier: res.data._ds.identifier,
        name: res.data._ds.name
      });
      setOwner({
        identifier: res.data._c.identifier,
        name: res.data._c.name
      });
      setShots(res.data._s);
      setCurrentShot(res.data._s[0]);
      setType(res.data._t);
      setMode(res.data._m);
      setDescription(res.data._d);
      setIsLoading(false);
    }).catch(e => {
      setIsLoading(true);
    });
  }, [identifier]);

  return (
    <SafeAreaView style={styles.defaultTabContainer}>
      <View style={styles.defaultTabHeader}>
        <TouchableOpacity
          style={styles.backArrow}
          onPress={() => navigation.goBack()}
          underlayColor='#fff'>
          <Ionicons name="ios-arrow-back" size={24} color={stylesheet.Tertiary} />
        </TouchableOpacity>
        <View style={styles.spacer}></View>
        <Text style={[styles.baseText, styles.bold, styles.tertiary]}>{name}</Text>
        <View style={styles.spacer}></View>
        <TouchableOpacity
          onPress={() => saveImage()}
          underlayColor='#fff'>
          <Ionicons name="md-download" size={24} color={stylesheet.Tertiary} />
        </TouchableOpacity>
      </View>
      {
        isLoading &&
        <View style={[styles.defaultLoader, styles.fullWidth, styles.fullHeight, styles.defaultColumnContainer]}>
          <View style={{flex: 0.1}}></View>
          <ActivityIndicator size="small" color={stylesheet.Primary} />
        </View>
      }
      <ScrollView style={styles.defaultTabScrollContent} contentContainerStyle={{alignItems: 'flex-start', minHeight: '100%', justifyContent: 'flex-start', width: '90%', marginLeft: '5%'}}>
        {
          wasDeleted &&
          <>
            <View style={{flex: 0.1}}></View>
            <Text style={[styles.subHeaderText, styles.centerText, styles.fullWidth, styles.primary]}>{Localization.t('designDeleted')}</Text>
          </>
        }
        {
          shots.length == 1 &&
          <Image
                style={styles.defaultZoomImage}
                source={{ uri: formats.cloudinarize(shots[0])}}
              />
        }
        {
          shots.length > 1 &&
          <TouchableOpacity style={styles.defaultZoomImageContainer} onPress={() => setCurrentShot(getNextShot(currentShot))}>
              <Image
                style={styles.defaultZoomMultiImage}
                source={{ uri: formats.cloudinarize(currentShot)}}
              />
              <View style={[{position: 'absolute', left: '25%', bottom: 10, backgroundColor: stylesheet.SecondaryTint, width: '50%', height: 3, borderRadius: 3}]}>
                <View style={{position: 'absolute', width: `${100 / shots.length}%`, left: `${(100 / shots.length) * getShotIndex(currentShot)}%`, borderRadius: 3, backgroundColor: stylesheet.Primary, height: '100%'}}></View>
              </View>
            </TouchableOpacity>
        }
        <View style={{flex: 0.1}}></View>
        <Text style={[styles.subHeaderText, styles.tertiary]}>{Localization.t('details')}</Text>
        <View style={{flex: 0.15}}></View>
        <Text style={[styles.baseText, styles.tertiary]}>{created}</Text>
        <View style={{flex: 0.02}}></View>
        <Text style={[styles.tinyText, styles.opaque, styles.tertiary]}>{Localization.t('created')}</Text>
        <View style={{flex: 0.08}}></View>
        <Text selectable={true} style={[styles.baseText, styles.tertiary]}>{description}</Text>
        <View style={{flex: 0.02}}></View>
        <Text style={[styles.tinyText, styles.opaque, styles.tertiary]}>{Localization.t('description')}</Text>
        <View style={{flex: 0.08}}></View>
        <Text style={[styles.baseText, styles.tertiary]}>{formats.getType(type)}</Text>
        <View style={{flex: 0.02}}></View>
        <Text style={[styles.tinyText, styles.opaque, styles.tertiary]}>{Localization.t('type')}</Text>
        <View style={{flex: 0.08}}></View>
        <Text style={[styles.baseText, styles.tertiary]}>{formats.getMode(mode)}</Text>
        <View style={{flex: 0.02}}></View>
        <Text style={[styles.tinyText, styles.opaque, styles.tertiary]}>{Localization.t('workshopMode')}</Text>
        {
          owner.identifier !== '' &&
          <>
            <View style={{flex: 0.1}}></View>
            <Text onPress={() => navigation.navigate('User', {session: SESSION, identifier: owner.identifier, name: owner.name})} style={[styles.baseText, styles.primary]}>{owner.name}</Text>
            <View style={{flex: 0.02}}></View>
            <Text style={[styles.tinyText, styles.opaque, styles.tertiary]}>{Localization.t('creator')}</Text>
          </>
        }
        {
          designer.identifier !== '' &&
          <>
            <View style={{flex: 0.1}}></View>
            <Text onPress={() => navigation.navigate('Brand', {session: SESSION, identifier: designer.identifier, name: designer.name})} style={[styles.baseText, styles.primary]}>{designer.name}</Text>
            <View style={{flex: 0.02}}></View>
            <Text style={[styles.tinyText, styles.opaque, styles.tertiary]}>{Localization.t('brand')}</Text>
          </>
        }
      </ScrollView>
    </SafeAreaView>
  );
}

const Styles = StyleSheet.create({
  singleImage: {
    width: '100%',
    height: 200,
    resizeMode: 'contain'
  },
  itemContainer: {
    width: Dimensions.get('window').width * 0.75,
    marginLeft: Dimensions.get('window').width * 0.025,
    marginRight: Dimensions.get('window').width * 0.025,
    height: 200,
    borderRadius: 30,
    flexDirection: 'column',
    backgroundColor: stylesheet.SecondaryTint
  }
});
