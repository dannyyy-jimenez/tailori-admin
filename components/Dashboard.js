import React from 'react';
import { View, SafeAreaView, Text, Switch, TextInput, TouchableOpacity } from 'react-native';
import { Ionicons, MaterialIcons } from '@expo/vector-icons';
import Localization from '../localization/config';
import { useIsFocused } from '@react-navigation/native';

import API from '../Api';

const stylesheet = require('../Styles');
const styles = stylesheet.Styles;
const formats = require('./Formats').Formats;

export default function Dashboard({navigation, route}) {
  const SESSION = route.params.session;

  const name = SESSION.name.split(' ')[0];
  const [releasesPending, setReleasesPending] = React.useState(0);
  const [ordersPending, setOrdersPending] = React.useState(0);
  const [usersAmount, setUsersAmount] = React.useState(0);
  const [daysTillFiling, setDaysTillFiling] = React.useState(-1);
  const [taxDue, setTaxDue] = React.useState(0);
  const [revenue, setRevenue] = React.useState(0);
  const [monthlyRevenue, setMonthlyRevenue] = React.useState(0);
  const [releasesAmount, setReleasesAmount] = React.useState(0);
  const [brandsAmount, setBrandsAmount] = React.useState(0);
  const [hasPermission, setHasPermission] = React.useState(true);

  React.useEffect(() => {
    API.get('dashboard', {auth: SESSION.crypto}).then((res) => {
      if (res.isError && res.response === 'NO_PERMISSION') throw 'permission';
      if (res.isError) throw 'error';

      setHasPermission(true);
      setUsersAmount(res.data._u);
      setMonthlyRevenue(res.data._mr);
      setReleasesAmount(res.data._r);
      setOrdersPending(res.data._op);
      setReleasesPending(res.data._rp);

      setDaysTillFiling(res.data._dtf);
      setTaxDue(res.data._td);
      setRevenue(res.data._rv);

      setBrandsAmount(res.data._b);
    }).catch((e) => {
      if (e === 'permission') {
        setHasPermission(false);
      }
    });
  }, [useIsFocused()])

  return (
    <SafeAreaView style={styles.defaultTabContainer}>
      <View style={styles.defaultTabHeader}>
        <View style={styles.spacer}></View>
        <Text style={[styles.baseText, styles.bold, styles.tertiary]}>{Localization.t('hello', {name})}</Text>
        <View style={styles.spacer}></View>
        <TouchableOpacity
          onPress={() => navigation.navigate("Logout")}
          underlayColor='#fff'>
          <Ionicons name="md-finger-print" size={24} color={stylesheet.Tertiary} />
        </TouchableOpacity>
      </View>
      <View style={[styles.defaultTabContent, styles.defaultRowContainer, styles.wrap, {justifyContent: 'space-evenly', alignItems: 'flex-start'}]}>
        {
          !hasPermission &&
          <>
            <View style={styles.spacer}></View>
            <Ionicons name="md-lock" size={48} color={stylesheet.Primary} />
            <View style={{flex: 0.05}}></View>
            <Text style={[styles.centerText, styles.primary, styles.bold, styles.baseText]}>{Localization.t('NO_PERMISSION')}</Text>
            <View style={styles.spacer}></View>
          </>
        }
        {
          hasPermission &&
          <>
            {
              daysTillFiling !== -1 &&
              <View style={[styles.statCard, styles.defaultColumnContainer, styles.center]}>
                <Text style={[styles.subHeaderText, styles.centerText, styles.manropeText, styles.fullWidth, styles.primary]}>${revenue}</Text>
                <View style={{flex: 0.1}}></View>
                <Text style={[styles.baseText, styles.primary, styles.centerText]}>Revenue</Text>
              </View>
            }
            {
              daysTillFiling !== -1 &&
              <View style={[styles.statCard, styles.defaultColumnContainer, styles.center]}>
                <Text style={[styles.subHeaderText, styles.centerText, styles.manropeText, styles.fullWidth, styles.primary]}>${taxDue}</Text>
                <View style={{flex: 0.1}}></View>
                <Text style={[styles.baseText, styles.primary, styles.centerText]}>Taxes Due</Text>
              </View>
            }
            {
              ordersPending !== 0 &&
              <TouchableOpacity onPress={() => navigation.navigate('Orders')} style={[styles.statCard, styles.defaultColumnContainer, styles.center]}>
                <Text style={[styles.megaText, styles.centerText, styles.manropeText, styles.fullWidth, styles.primary]}>{ordersPending}</Text>
                <View style={{flex: 0.1}}></View>
                <Text style={[styles.baseText, styles.primary, styles.centerText]}>{Localization.t('ordersPending')}</Text>
              </TouchableOpacity>
            }
            {
              releasesPending !== 0 &&
              <TouchableOpacity onPress={() => navigation.navigate('Releases')} style={[styles.statCard, styles.defaultColumnContainer, styles.center]}>
                <Text style={[styles.megaText, styles.centerText, styles.manropeText, styles.fullWidth, styles.primary]}>{releasesPending}</Text>
                <View style={{flex: 0.1}}></View>
                <Text style={[styles.baseText, styles.primary, styles.centerText]}>{Localization.t('releasesPending')}</Text>
              </TouchableOpacity>
            }
            <TouchableOpacity style={[styles.statCard, styles.defaultColumnContainer, styles.center]}>
              <Text style={[styles.subHeaderText, styles.centerText, styles.manropeText, styles.fullWidth, styles.tertiary]}>${monthlyRevenue}</Text>
              <View style={{flex: 0.1}}></View>
              <Text style={[styles.baseText, styles.tertiary]}>{Localization.t('monthlyRevenue')}</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => navigation.navigate('Users')} style={[styles.statCard, styles.defaultColumnContainer, styles.center]}>
              <Text style={[styles.megaText, styles.centerText, styles.manropeText, styles.fullWidth, styles.tertiary]}>{usersAmount}</Text>
              <View style={{flex: 0.1}}></View>
              <Text style={[styles.baseText, styles.tertiary]}>{Localization.t('users')}</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => navigation.navigate('Brands')} style={[styles.statCard, styles.defaultColumnContainer, styles.center]}>
              <Text style={[styles.megaText, styles.centerText, styles.manropeText, styles.fullWidth, styles.tertiary]}>{brandsAmount}</Text>
              <View style={{flex: 0.1}}></View>
              <Text style={[styles.baseText, styles.tertiary]}>{Localization.t('brands')}</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => navigation.navigate('Releases')} style={[styles.statCard, styles.defaultColumnContainer, styles.center]}>
              <Text style={[styles.megaText, styles.centerText, styles.manropeText, styles.fullWidth, styles.tertiary]}>{releasesAmount}</Text>
              <View style={{flex: 0.1}}></View>
              <Text style={[styles.baseText, styles.tertiary]}>{Localization.t('releases')}</Text>
            </TouchableOpacity>
          </>
        }
      </View>
    </SafeAreaView>
  );
}
