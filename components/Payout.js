import React from 'react';
import { View, TouchableWithoutFeedback, Keyboard, SafeAreaView, Text, ActivityIndicator, TextInput, TouchableOpacity } from 'react-native';
import { Ionicons, FontAwesome5 } from '@expo/vector-icons';

import API from '../Api';

const stylesheet = require('../Styles');
const styles = stylesheet.Styles;
const formats = require('./Formats').Formats;

export default function Update({navigation, route}) {
  const SESSION = route.params.session;

  const identifier = route.params.identifier;
  const [isLoading, setIsLoading] = React.useState(true);
  const [hasPermission, setHasPermission] = React.useState(false);
  const [amount, setAmount] = React.useState('');
  const [account, setAccount] = React.useState(false);
  const [fulfilled, setFulfilled] = React.useState(false);
  const [accounts, setAccounts] = React.useState([]);

  const onPayout = async () => {
    setIsLoading(true);

    try {
      const body = {auth: SESSION.crypto, identifier, amount, account: account.id};
      const res = await API.post('order/payout', body);
      if (res.isError) throw 'error';

      setIsLoading(false);
      setFulfilled(true);
    } catch(e) {
      setIsLoading(false);
    }
  };

  React.useEffect(() => {
    setIsLoading(true);

    API.get('order/payees', {auth: SESSION.crypto, identifier: identifier}).then(res => {
      if (res.isError) throw 'error';

      setHasPermission(res.data._p);
      setIsLoading(false);
      setAccounts(res.data._a);
    }).catch(e => {
      navigation.goBack()
    })
  }, [identifier])

  return (
    <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
      <SafeAreaView style={styles.defaultTabContainer}>
        <View style={styles.defaultTabHeader}>
          <TouchableOpacity
            onPress={() => navigation.goBack()}
            underlayColor='#fff'>
            <Ionicons name="ios-arrow-back" size={24} color={stylesheet.Tertiary} />
          </TouchableOpacity>
          <View style={styles.spacer}></View>
          {
            !account &&
            <Text style={[styles.baseText, styles.bold, styles.tertiary]}>Payout #{identifier}</Text>
          }
          {
            account &&
            <Text style={[styles.baseText, styles.bold, styles.tertiary]}>Payout {account.name}</Text>
          }
          <View style={styles.spacer}></View>
        </View>
        {
          isLoading &&
          <View style={[styles.defaultLoader, styles.fullWidth, styles.fullHeight, styles.defaultColumnContainer]}>
            <View style={{flex: 0.1}}></View>
            <ActivityIndicator size="small" color={stylesheet.Primary} />
          </View>
        }
        <View style={styles.defaultTabContent}>
          {
            !hasPermission &&
            <>
              <Ionicons name="md-lock" size={64} color={stylesheet.Primary} />
              <View style={{flex: 0.1}}></View>
              <Text style={[styles.baseText, styles.centerText, styles.bold, styles.fullWidth, styles.primary]}>You cannot provide payouts</Text>
            </>
          }
          {
            hasPermission && !account &&
            <>
              {
                accounts.map((account, index) => {
                  return (
                    <>
                      <TouchableOpacity
                        key={index}
                        style={[styles.roundedButton, styles.filled]}
                        onPress={() => setAccount(account)}
                        underlayColor='#fff'>
                        <Text style={[styles.secondary, styles.baseText, styles.centerText, styles.bold]}>{account.name}</Text>
                      </TouchableOpacity>
                      <View key={"SPACER"+index} style={{flex: 0.1}}></View>
                    </>
                  )
                })
              }
            </>
          }
          {
            hasPermission && account && !fulfilled &&
            <>
              <View style={{flex: 0.15}}></View>
              <TextInput style={[styles.baseInput, styles.filledInput, styles.tertiary]} value={amount} onChangeText={value => setAmount(value)} keyboardType="decimal-pad" placeholder="Enter Amount..." placeholderTextColor="#555555" returnKeyType="go" selectionColor="#DB113B" textContentType="none" />
              <View style={{flex: 0.10}}></View>
              <TouchableOpacity
                disabled={isLoading || !account || !hasPermission}
                style={[styles.roundedButton, styles.filled, !hasPermission || isLoading || !account ? styles.disabled : {opacity: 1}]}
                onPress={() => onPayout()}
                underlayColor='#fff'>
                <Text style={[styles.secondary, styles.baseText, styles.centerText, styles.bold]}>Send</Text>
              </TouchableOpacity>
              <View style={{flex: 0.1}}></View>
            </>
          }
          {
            fulfilled &&
            <>
              <FontAwesome5 name="money-check-alt" size={64} color={stylesheet.Primary} />
              <View style={{flex: 0.1}}></View>
              <Text style={[styles.baseText, styles.centerText, styles.fullWidth, styles.tertiary]}>Awesome! <Text style={styles.primary}>{account.name}</Text> received <Text style={styles.primary}>${parseFloat(amount).toFixed(2)}</Text> payout</Text>
            </>
          }
        </View>
      </SafeAreaView>
    </TouchableWithoutFeedback>
  );
}
