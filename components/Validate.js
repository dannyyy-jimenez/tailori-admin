import React from 'react';
import { View, SafeAreaView, Text, TouchableOpacity, Switch, Keyboard, TextInput, KeyboardAvoidingView, TouchableWithoutFeedback } from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import Localization from '../localization/config';

const stylesheet = require('../Styles');
const styles = stylesheet.Styles;

export default function Validate({navigation, route}) {
  const [price, setPrice] = React.useState('');
  const onBack = (decision) => {
    route.params.onValidate({price, decision});
    navigation.goBack();
  };

  return (
    <SafeAreaView style={styles.defaultTabContainer}>
      <View style={styles.defaultTabHeader}>
        <TouchableOpacity
          onPress={() => onBack()}
          underlayColor='#fff'>
          <Ionicons name="ios-arrow-back" size={24} color={stylesheet.Tertiary} />
        </TouchableOpacity>
        <View style={styles.spacer}></View>
        <Text style={[styles.baseText, styles.bold, {color: '#fc0330'}]}>Trinum Design</Text>
        <View style={styles.spacer}></View>
      </View>
      <KeyboardAvoidingView behavior="padding" style={styles.defaultTabContainer}>
        <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
          <View style={{
            flexDirection: 'column',
            alignItems: 'center',
            justifyContent: 'center',
            height: '100%',
            width: '100%'
          }}>
            <View style={{flex: 0.15}}></View>
            <Text style={[styles.centerText, styles.tertiary, styles.bold, styles.subHeaderText]}>{Localization.t("releaseDecision")}</Text>
            <View style={styles.spacer}></View>
            <TextInput style={[styles.baseInput, styles.filledInput, styles.tertiary]} value={price} onChangeText={value => setPrice(value)} keyboardType="decimal-pad" placeholder={Localization.t('price')} placeholderTextColor="#555555" returnKeyType="done" selectionColor="#DB113B" textContentType="none" />
            <View style={styles.spacer}></View>
            <TouchableOpacity
              disabled={price === ''}
              style={[styles.roundedButton, styles.filled, price === '' ? styles.disabled : {opacity: 1}]}
              onPress={() => onBack(true)}
              underlayColor='#fff'>
              <Text style={[styles.secondary, styles.baseText, styles.centerText, styles.bold]}>{Localization.t('approve')}</Text>
            </TouchableOpacity>
            <View style={{flex: 0.1}}></View>
            <TouchableOpacity
              style={[styles.roundedButton, styles.clear]}
              onPress={() => onBack(false)}
              underlayColor='#fff'>
              <Text style={[{color: 'red'}, styles.baseText, styles.centerText]}>{Localization.t('deny')}</Text>
            </TouchableOpacity>
            <View style={{flex: 0.1}}></View>
          </View>
        </TouchableWithoutFeedback>
      </KeyboardAvoidingView>
    </SafeAreaView>
  );
}
