import React from 'react';
import { View, SafeAreaView, Text, ScrollView, ActivityIndicator, TouchableOpacity } from 'react-native';
import { Ionicons, MaterialIcons, FontAwesome5 } from '@expo/vector-icons';
import Clipboard from 'expo-clipboard';
import Localization from '../localization/config';

import API from '../Api';

const stylesheet = require('../Styles');
const styles = stylesheet.Styles;
const formats = require('./Formats').Formats;

export default function Brand({navigation, route}) {
  const SESSION = route.params.session;

  const {name, identifier} = route.params;
  const [isLoading, setIsLoading] = React.useState(true);
  const [lastUpdated, setLastUpdated] = React.useState('');
  const [description, setDescription] = React.useState('');
  const [payouts, setPayouts] = React.useState(0);
  const [distribution, setDistribution] = React.useState(0);
  const [releases, setReleases] = React.useState(0);
  const [owner, setOwner] = React.useState({
    identifier: '',
    name: ''
  });

  const onShare = async () => {
    Clipboard.setString(`https://www.tailorii.app/shop/brand/${identifier}`);
    alert('Copied Link!');
  }

  React.useEffect(() => {
    API.get('brand', {auth: SESSION.crypto, identifier: identifier}).then((res) => {
      if (res.isError) throw 'error';

      setLastUpdated(res.data._lu);
      setDescription(res.data._d);
      setPayouts(res.data._p);
      setDistribution(res.data._db);
      setReleases(res.data._r);
      setOwner({
        identifier: res.data._o.identifier,
        name: res.data._o.name
      })
      setIsLoading(false);
    }).catch(e => {
      setIsLoading(true);
    });
  }, [identifier]);

  return (
    <SafeAreaView style={styles.defaultTabContainer}>
      <View style={styles.defaultTabHeader}>
        <TouchableOpacity
          style={styles.backArrow}
          onPress={() => navigation.goBack()}
          underlayColor='#fff'>
          <Ionicons name="ios-arrow-back" size={24} color={stylesheet.Tertiary} />
        </TouchableOpacity>
        <View style={styles.spacer}></View>
        <Text style={[styles.baseText, styles.bold, styles.tertiary]}>{name}</Text>
        <View style={styles.spacer}></View>
        <TouchableOpacity
          onPress={() => onShare()}
          underlayColor='#fff'>
          <FontAwesome5 name="share" size={24} color={stylesheet.Tertiary} />
        </TouchableOpacity>
      </View>
      {
        isLoading &&
        <View style={[styles.defaultLoader, styles.fullWidth, styles.fullHeight, styles.defaultColumnContainer]}>
          <View style={{flex: 0.1}}></View>
          <ActivityIndicator size="small" color={stylesheet.Primary} />
        </View>
      }
      <ScrollView style={styles.defaultTabScrollContent} contentContainerStyle={{alignItems: 'flex-start', minHeight: '100%', justifyContent: 'flex-start', width: '90%', marginLeft: '5%'}}>
        <View style={{flex: 0.1}}></View>
        <Text style={[styles.subHeaderText, styles.tertiary]}>{Localization.t('details')}</Text>
        <View style={{flex: 0.05}}></View>
        <Text style={[styles.baseText, styles.tertiary]}>{lastUpdated}</Text>
        <View style={{flex: 0.02}}></View>
        <Text style={[styles.tinyText, styles.opaque, styles.tertiary]}>{Localization.t('lastUpdated')}</Text>
        <View style={{flex: 0.08}}></View>
        <Text selectable={true} style={[styles.baseText, styles.tertiary]}>{identifier}</Text>
        <View style={{flex: 0.02}}></View>
        <Text style={[styles.tinyText, styles.opaque, styles.tertiary]}>{Localization.t('identifier')}</Text>
        <View style={{flex: 0.08}}></View>
        <Text selectable={true} style={[styles.baseText, styles.tertiary]}>{description}</Text>
        <View style={{flex: 0.02}}></View>
        <Text style={[styles.tinyText, styles.opaque, styles.tertiary]}>{Localization.t('description')}</Text>
        <View style={{flex: 0.08}}></View>
        <Text selectable={true} style={[styles.baseText, styles.tertiary]}>{formats.commasize(releases)}</Text>
        <View style={{flex: 0.02}}></View>
        <Text style={[styles.tinyText, styles.opaque, styles.tertiary]}>{Localization.t('releases')}</Text>
        <View style={{flex: 0.08}}></View>
        <Text selectable={true} style={[styles.baseText, styles.tertiary]}>${formats.commasize(payouts)}</Text>
        <View style={{flex: 0.02}}></View>
        <Text style={[styles.tinyText, styles.opaque, styles.tertiary]}>{Localization.t('payouts')}</Text>
        <View style={{flex: 0.08}}></View>
        <Text selectable={true} style={[styles.baseText, styles.tertiary]}>{formats.commasize(distribution)}</Text>
        <View style={{flex: 0.02}}></View>
        <Text style={[styles.tinyText, styles.opaque, styles.tertiary]}>{Localization.t('distribution')}</Text>
        <View style={{flex: 0.1}}></View>
        <Text onPress={() => navigation.navigate('User', {session: SESSION, identifier: owner.identifier, name: owner.name})} style={[styles.baseText, styles.tertiary]}>{owner.name}</Text>
        <View style={{flex: 0.02}}></View>
        <Text style={[styles.tinyText, styles.opaque, styles.tertiary]}>{Localization.t('creator')}</Text>
      </ScrollView>
    </SafeAreaView>
  );
}
