import Localization from '../localization/config';

export const Formats = {
  formatPhoneNumber : (phoneNumberString) => {
    var cleaned = ('' + phoneNumberString).replace(/\D/g, '')
    var match = cleaned.match(/^(\d{3})(\d{1,3})?(\d{1,4})?$/)
    if (match) {
      return '(' + match[1] + (match[1] && match[2] ? ') ' : '') + (match[2] || '') + (match[2] && match[3] ? '-' : '') + (match[3] || '')
    }
    return phoneNumberString;
  },
  formatDob : (dobString) => {
    var cleaned = ('' + dobString).replace(/\D/g, '')
    var match = cleaned.match(/^(0[1-9]|1[0-2])(0[1-9]?|1\d?|2\d?|3[01]?)((19?\d{0,2})|(20?\d{0,2}))?$/);
    if (match) {
      return match[1] + (match[2] ? '/' + match[2] : '') + (match[3] && match[2] && match[2].length == 2 ? '/' + match[3] : '');
    }
    return dobString;
  },
  ccNumber: (number) => {
    number = number.replace(/\D/g, '');
    let groups = number.match(/.{1,4}/g);
    if (!groups) return '';
    return groups.join(' ');
  },
  ccExp: (date, previous) => {
    if (date.length < previous.length && date.length == 2) {
      // backspace
      date = date.substring(0,1);
    } else if (date.length == 2) {
      date += '/';
    }

    return date;
  },
  ccExpPretty: (date) => {
    let parts = date.split('/');

    if (parts[1] && parts[1].length > 2) {
      parts[1] = parts[1].substring(2);
    }

    return parts.join('/');
  },
  cardIcon: (number) => {
    if (number.match(/^4/g) != null)
        return "cc-visa";

    if (/^(5[1-5][0-9]{14}|2(22[1-9][0-9]{12}|2[3-9][0-9]{13}|[3-6][0-9]{14}|7[0-1][0-9]{13}|720[0-9]{12}))$/.test(number))
        return "cc-mastercard";

    if (number.match(/^3[47]/g) != null)
        return "cc-amex";

    let re = new RegExp("^(6011|622(12[6-9]|1[3-9][0-9]|[2-8][0-9]{2}|9[0-1][0-9]|92[0-5]|64[4-9])|65)");
    if (number.match(re) != null)
      return "cc-discover";

    re = new RegExp("^36");
    if (number.match(re) != null)
      return "cc-diners-club";

    re = new RegExp("^35(2[89]|[3-8][0-9])");
    if (number.match(re) != null)
      return "cc-jcb";

    return 'credit-card';
  },
  abbreviate: (quantity, monetary = false) => {
    if (quantity >= 1000000000) {
      return `${(quantity / 1000000000).toFixed(1)}B`;
    } else if (quantity >= 1000000) {
      return `${(quantity / 1000000).toFixed(1)}B`;
    } else if (quantity >= 10000) {
      return `${(quantity / 1000).toFixed(1)}K`;
    }
    return monetary ? quantity.toFixed(2) : quantity.toString();
  },
  commasize: (quantity) => {
    return quantity.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  },
  cloudinarize: (uri, query = '') => {
    return `https://res.cloudinary.com/lgxy/image/upload/${query}${uri}`;
  },
  getType: (type) => {
    if (type === "shirtTxB") return Localization.t('shirtTxB');
    if (type === "shirtTsleeve") return Localization.t('shirtTsleeve');
    if (type === "shirtT") return Localization.t('shirtT');
    if (type.includes("shirt")) return Localization.t('shirt');
    if (type.includes("hoodie")) return Localization.t('hoodie');
    if (type === "popsocket") return Localization.t('popsocket');
    if (type === "mug") return Localization.t('mug');
    if (type === "pillow") return Localization.t('pillow');
    if (type === "keychain") return Localization.t('keychain')
    if (type === "crewneck") return Localization.t('crewneck');

    return Localization.t('item');
  },
  getMode: (mode) => {
    if (mode === 'upload') return 'Upload';

    return 'Workshop';
  },
  printStyleNice: (style) => {
    if (style === 'SCPT') return 'Screenprint';
    if (style === 'VNYL') return 'Vinyl';

    return '';
  },
  formatSide: (side) => {
    return Localization.t(side, { defaultValue: "" });
  }
}
