import React from 'react';
import { View, SafeAreaView, Text, RefreshControl, Image, ActivityIndicator, StyleSheet, ScrollView, TextInput, TouchableOpacity } from 'react-native';
import { Ionicons, MaterialIcons } from '@expo/vector-icons';
import ActionSheet from "react-native-actions-sheet";
import Localization from '../localization/config';

import API from '../Api';
import Release from '../models/Release';

const stylesheet = require('../Styles');
const styles = stylesheet.Styles;
const formats = require('./Formats').Formats;

const actionSheetRef = React.createRef();

export default function Releases({navigation, route}) {
  const SESSION = route.params.session;

  const [refreshing, setRefreshing] = React.useState(false);
  const [isLoading, setIsLoading] = React.useState(true);
  const [releases, setReleases] = React.useState([]);
  const [hasPermission, setHasPermission] = React.useState(false);
  const [sortBy, setSortBy] = React.useState('oldest');
  const [displayApproved, setDisplayApproved] = React.useState(false);

  const load = async () => {
    setReleases([]);
    setIsLoading(true);

    try {
      const res =  await API.get('releases', {auth: SESSION.crypto, sortBy, displayApproved});
      if (res.isError && res.response === 'NO_PERMISSION') throw 'permission';

      if (res.isError) throw 'error';

      setHasPermission(true);
      setReleases(res.data._r.map(release => new Release(release.identifier, release.cover, release.name, release.price, release.date, release.approved)));
      setIsLoading(false);
    } catch (e) {
      if (e === 'permission') {
        setHasPermission(false);
      }
      setIsLoading(false);
    };
  };

  const refresh = async() => {
    setRefreshing(true);
    await load();
    setRefreshing(false);
  }

  React.useEffect(() => {
    load();
  }, [sortBy, displayApproved]);

  return (
    <SafeAreaView style={styles.defaultTabContainer}>
      <View style={styles.defaultTabHeader}>
        <TouchableOpacity
          onPress={() => actionSheetRef.current?.setModalVisible(true)}
          underlayColor='#fff'>
          <MaterialIcons name="sort" size={24} color={stylesheet.Primary} />
        </TouchableOpacity>
        <View style={styles.spacer}></View>
        <Text style={[styles.baseText, styles.bold, styles.tertiary]}>{Localization.t('releases')}</Text>
        <View style={styles.spacer}></View>
        <TouchableOpacity
          style={styles.backArrow}
          onPress={() => navigation.navigate('Designs')}
          underlayColor='#fff'>
          <Ionicons name="md-brush" size={24} color={stylesheet.Tertiary} />
        </TouchableOpacity>
      </View>
      {
        isLoading &&
        <View style={[styles.defaultLoader, styles.fullWidth, styles.fullHeight, styles.defaultColumnContainer]}>
          <View style={{flex: 0.1}}></View>
          <ActivityIndicator size="small" color={stylesheet.Primary} />
        </View>
      }
      <ScrollView style={styles.defaultTabScrollContent} contentContainerStyle={{alignItems: 'center', justifyContent: 'flex-start', width: '90%', marginLeft: '5%'}} refreshControl={<RefreshControl refreshing={refreshing} tintColor={stylesheet.Primary} colors={[stylesheet.Primary]} onRefresh={load} />}>
        <View style={{flex: 0.05}}></View>
        {
          !hasPermission &&
          <>
            <View style={styles.spacer}></View>
            <Ionicons name="md-lock" size={48} color={stylesheet.Primary} />
            <View style={{flex: 0.05}}></View>
            <Text style={[styles.centerText, styles.primary, styles.bold, styles.baseText]}>{Localization.t('NO_PERMISSION')}</Text>
            <View style={styles.spacer}></View>
          </>
        }
        {
          releases.map((release) => {
            return (
              <TouchableOpacity key={release.getIdentifier()} onPress={() => navigation.navigate('Release', {session: SESSION, identifier: release.getIdentifier()})} style={[Styles.itemContainer, !release.wasApproved() ? {opacity: 1} : {opacity: 0.2}]}>
                <Image style={Styles.itemImage} source={{uri: formats.cloudinarize(release.getCover())}}/>
                <View style={{flex: 0.02}}></View>
                <View style={[styles.defaultColumnContainer, styles.spacer]}>
                  <View style={styles.defaultRowContainer}>
                    <Text style={[styles.baseText, styles.bold, styles.tertiary]}>{release.getName()}</Text>
                    <View style={styles.spacer}></View>
                    <Text style={[styles.tinyText, styles.opaque, styles.tertiary]}>${release.getPrice()}</Text>
                  </View>
                  <View style={styles.spacer}></View>
                  <View style={styles.defaultRowContainer}>
                    <Text style={[styles.tinyText, styles.bold, styles.opaque, styles.tertiary]}>#{release.getIdentifier()}</Text>
                    <View style={styles.spacer}></View>
                    <Text style={[styles.tinyText, styles.opaque, styles.tertiary]}>{release.getDate()}</Text>
                  </View>
                </View>
              </TouchableOpacity>
            )
          })
        }
      </ScrollView>
      <ActionSheet containerStyle={{paddingBottom: 20, backgroundColor: stylesheet.Secondary}} indicatorColor={stylesheet.Primary} gestureEnabled={true} ref={actionSheetRef}>
        <View>
          <Text style={[styles.baseText, styles.fullWidth, styles.bold, styles.centerText, styles.tertiary, {marginTop: 10}]}>{Localization.t('filters')}</Text>
          <View style={styles.line}></View>
          <Text style={[styles.baseText, styles.marginWidth, styles.bold, styles.tertiary, {marginTop: 10, marginBottom: 10}]}>{Localization.t('sortBy')}</Text>
          <View style={[styles.paddedWidth, styles.defaultColumnContainer]}>
            <TouchableOpacity onPress={() => setSortBy('oldest')} disabled={sortBy === 'oldest'} style={[styles.defaultRowContainer, styles.actionListItem, sortBy === 'oldest' ? styles.disabled : {}]}>
              <Text style={[styles.spacer, styles.baseText, styles.tertiary]}>{Localization.t('oldest')}</Text>
              {
                sortBy === 'oldest' &&
                <Ionicons name="md-checkmark" size={18} color={stylesheet.Secondary} />
              }
            </TouchableOpacity>
          </View>
          <View style={[styles.paddedWidth, styles.defaultColumnContainer]}>
            <TouchableOpacity onPress={() => setSortBy('newest')} disabled={sortBy === 'newest'} style={[styles.defaultRowContainer, styles.actionListItem, sortBy === 'newest' ? styles.disabled : {}]}>
              <Text style={[styles.spacer, styles.baseText, styles.tertiary]}>{Localization.t('newest')}</Text>
              {
                sortBy === 'newest' &&
                <Ionicons name="md-checkmark" size={18} color={stylesheet.Secondary} />
              }
            </TouchableOpacity>
          </View>

          <Text style={[styles.baseText, styles.marginWidth, styles.bold, styles.tertiary, {marginTop: 20, marginBottom: 10}]}>{Localization.t('displayApproved')}</Text>
          <View style={[styles.paddedWidth, styles.defaultColumnContainer]}>
            <TouchableOpacity onPress={() => setDisplayApproved(false)} disabled={!displayApproved} style={[styles.defaultRowContainer, styles.actionListItem, !displayApproved ? styles.disabled : {}]}>
              <Text style={[styles.spacer, styles.baseText, styles.tertiary]}>{Localization.t('no')}</Text>
              {
                !displayApproved &&
                <Ionicons name="md-checkmark" size={18} color={stylesheet.Secondary} />
              }
            </TouchableOpacity>
          </View>
          <View style={[styles.paddedWidth, styles.defaultColumnContainer]}>
            <TouchableOpacity onPress={() => setDisplayApproved(true)} disabled={displayApproved} style={[styles.defaultRowContainer, styles.actionListItem, displayApproved ? styles.disabled : {}]}>
              <Text style={[styles.spacer, styles.baseText, styles.tertiary]}>{Localization.t('yes')}</Text>
              {
                displayApproved &&
                <Ionicons name="md-checkmark" size={18} color={stylesheet.Secondary} />
              }
            </TouchableOpacity>
          </View>
        </View>
      </ActionSheet>
    </SafeAreaView>
  );
}

const Styles = StyleSheet.create({
  itemContainer: {
    width: 300,
    height: 250,
    borderRadius: 10,
    padding: 10,
    margin: 10,
    flexDirection: 'column',
    justifyContent: 'center',
    backgroundColor: stylesheet.SecondaryTint
  },
  itemImage: {
    height: '83%',
    width: 'auto',
    resizeMode: 'contain'
  }
});
