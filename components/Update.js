import React from 'react';
import { View, SafeAreaView, Text, ActivityIndicator, TextInput, TouchableOpacity } from 'react-native';
import { Ionicons, MaterialIcons } from '@expo/vector-icons';
import Localization from '../localization/config';

import API from '../Api';

const stylesheet = require('../Styles');
const styles = stylesheet.Styles;
const formats = require('./Formats').Formats;

export default function Update({navigation, route}) {
  const SESSION = route.params.session;

  const identifier = route.params.identifier;
  const [isLoading, setIsLoading] = React.useState(true);
  const [hasPermission, setHasPermission] = React.useState(false);
  const [message, setMessage] = React.useState('');
  const [response, setResponse] = React.useState('');

  const onUpdate = async () => {
    setIsLoading(true);

    try {
      const body = {auth: SESSION.crypto, identifier, messageType: 'custom', message};
      const res = await API.post('order/update', body);
      if (res.isError) throw 'error';

      setMessage('');
      setResponse(res.data._r);
      setIsLoading(false);
    } catch(e) {
      setIsLoading(false);
    }
  };

  React.useEffect(() => {
    setIsLoading(true);

    API.get('order/update', {auth: SESSION.crypto, identifier: identifier}).then(res => {
      if (res.isError) throw 'error';

      setHasPermission(res.data._p);
      setIsLoading(false);
    }).catch(e => {
      console.log(e);
      navigation.goBack()
    })
  }, [identifier])

  return (
    <SafeAreaView style={styles.defaultTabContainer}>
      <View style={styles.defaultTabHeader}>
        <TouchableOpacity
          onPress={() => navigation.goBack()}
          underlayColor='#fff'>
          <Ionicons name="ios-arrow-back" size={24} color={stylesheet.Tertiary} />
        </TouchableOpacity>
        <View style={styles.spacer}></View>
        <Text style={[styles.baseText, styles.bold, styles.tertiary]}>{Localization.t('update')} #{identifier}</Text>
        <View style={styles.spacer}></View>
      </View>
      {
        isLoading &&
        <View style={[styles.defaultLoader, styles.fullWidth, styles.fullHeight, styles.defaultColumnContainer]}>
          <View style={{flex: 0.1}}></View>
          <ActivityIndicator size="small" color={stylesheet.Primary} />
        </View>
      }
      <View style={styles.defaultTabContent}>
        {
          !hasPermission &&
          <>
            <View style={{flex: 0.1}}></View>
            <Text style={[styles.baseText, styles.centerText, styles.fullWidth, styles.primary]}>{Localization.t('NO_PERMISSION_UPDATE')}</Text>
          </>
        }
        <View style={styles.spacer}></View>
        {
          response !== '' &&
          <>
            <Text selectable={true} style={[styles.baseText, styles.centerText, styles.marginWidth, styles.primary]}>{response}</Text>
            <View style={{flex: 0.1}}></View>
          </>
        }
        <TextInput style={[styles.baseInput, styles.filledInput, styles.tertiary]} value={message} onChangeText={value => setMessage(value)} keyboardType="default" placeholder={Localization.t('enterMessage')} placeholderTextColor="#555555" returnKeyType="go" selectionColor="#DB113B" textContentType="none" />
        <View style={styles.spacer}></View>
        <Text style={[styles.tinyText, styles.marginWidth, styles.primary]}>BOXED - Print Shipping Label</Text>
        <View style={{flex: 0.02}}></View>
        <Text style={[styles.tinyText, styles.marginWidth, styles.primary]}>DROPPED - Dropped off</Text>
        <View style={{flex: 0.1}}></View>
        <TouchableOpacity
          disabled={isLoading || message === '' || !hasPermission}
          style={[styles.roundedButton, styles.filled, !hasPermission || isLoading || message === '' ? styles.disabled : {opacity: 1}]}
          onPress={() => onUpdate()}
          underlayColor='#fff'>
          <Text style={[styles.secondary, styles.baseText, styles.centerText, styles.bold]}>{Localization.t('send')}</Text>
        </TouchableOpacity>
        <View style={{flex: 0.1}}></View>
      </View>
    </SafeAreaView>
  );
}
