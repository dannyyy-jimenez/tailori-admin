import React from 'react';
import { StyleSheet, Text, View, Keyboard, Image, KeyboardAvoidingView, TouchableWithoutFeedback, TouchableOpacity, TextInput } from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import Localization from '../localization/config';

import API from '../Api';

const stylesheet = require('../Styles');
const styles = require('../Styles').Styles;
const formats = require('./Formats').Formats;

export default function Login({navigation, route}) {
  const [phoneNumber, setPhoneNumber] = React.useState('');
  const [password, setPassword] = React.useState('');
  const [canSubmit, setCanSubmit] = React.useState(false);
  const [isLoading, setIsLoading] = React.useState(false);
  const [isError, setIsError] = React.useState(false);

  const onNumberChange = (value) => {
    setPhoneNumber(formats.formatPhoneNumber(value));
    setCanSubmit(value.length > 5 && password.length > 5);
    setIsError(false);
  };

  const onPasswordChange = (value) => {
    setPassword(value);
    setCanSubmit(value.length > 5 && phoneNumber.length > 5);
    setIsError(false);
  };

  const onLogin = () => {
    setIsLoading(true);
    Keyboard.dismiss();

    const body = { phoneNumber, password };

    API.post('login', body).then(res => {
      if (res.isError) {
        setIsLoading(false);
        setIsError(true);
        setCanSubmit(false);
        return;
      }
      setPhoneNumber('');
      setPassword('');
      setCanSubmit(false);
      navigation.navigate('Auth', {name: res.data.fullName, crypto: res.data.crypto});
      setIsLoading(false);

    }).catch(() => setIsLoading(false));
  };

  return (
    <KeyboardAvoidingView behavior="height" style={styles.defaultTabContainer, {width: '100%', height: '100%'}}>
      <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
        <View style={{
          flexDirection: 'column',
          alignItems: 'center',
          justifyContent: 'center',
          height: '100%',
          width: '100%'
        }}>
          <View style={styles.spacer}></View>
          <Ionicons name="md-lock-closed" size={96} color={stylesheet.Primary} />
          <View style={{flex: 0.1}}></View>
          <TextInput style={[styles.baseInput, styles.tertiary]} value={phoneNumber} onChangeText={value => onNumberChange(value)} keyboardType="number-pad" placeholder={Localization.t('phoneNumber')} placeholderTextColor="#555555" returnKeyType="next" selectionColor="#DB113B" textContentType="telephoneNumber" />
          <View style={{flex: 0.1}}></View>
          <TextInput style={[styles.baseInput, styles.fullWidth, styles.tertiary]} value={password} onChangeText={value => onPasswordChange(value)} placeholder={Localization.t('password')} placeholderTextColor="#555555" returnKeyType="go" selectionColor="#DB113B" secureTextEntry={true} textContentType="password" />
          {
            isError &&
            <>
              <View style={{flex: 0.1}}></View>
              <Text style={[styles.centerText, styles.primary, styles.baseText]}>{Localization.t('invalidCredentials')}</Text>
            </>
          }
          <View style={styles.spacer}></View>
          <TouchableOpacity
            disabled={!canSubmit || isLoading}
            style={[styles.roundedButton, styles.filled, !canSubmit || isLoading ? styles.disabled : {opacity: 1}]}
            onPress={() => onLogin()}
            underlayColor='#fff'>
            <Text style={[styles.secondary, styles.baseText, styles.centerText, styles.bold]}>{Localization.t('login')}</Text>
          </TouchableOpacity>
          <View style={{flex: 0.1}}></View>
        </View>
      </TouchableWithoutFeedback>
    </KeyboardAvoidingView>
  );
}
