import Constants from 'expo-constants';

const ENV = {
  apiUrl: (Constants.debugMode === true || Constants.executionEnvironment !== 'bare') ? 'http://10.0.0.102:80/api/lgxy/' : 'https://www.tailorii.app/api/lgxy' // http://10.0.0.102:80 https://www.tailorii.app
};

export default ENV;
